package com.epam.authorization.service;

import java.time.Instant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.stereotype.Component;

import com.epam.authorization.config.RsaKeyProperties;

import io.jsonwebtoken.Jwts;


@Component
public class JwtService {


	private final JwtEncoder encoder;
	
	@Autowired
	private RsaKeyProperties rsaKeyProperties;

	public JwtService(JwtEncoder encoder) {
		this.encoder = encoder;
	}
	
	public void validateToken(final String token) {
        Jwts.parserBuilder().setSigningKey(rsaKeyProperties.privateKey()).build().parseClaimsJws(token);
    }

	public String generateToken(String userName) {
		Instant now = Instant.now();
		long expiry = 60000L;
		JwtClaimsSet claims = JwtClaimsSet.builder().issuer("EPAM Systems").issuedAt(now)
				.expiresAt(now.plusSeconds(expiry)).subject(userName).build();
		return this.encoder.encode(JwtEncoderParameters.from(claims)).getTokenValue();
	}
}