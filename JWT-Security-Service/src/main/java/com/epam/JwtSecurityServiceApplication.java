package com.epam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.epam.authorization.config.RsaKeyProperties;

@SpringBootApplication
@EnableConfigurationProperties(RsaKeyProperties.class)
public class JwtSecurityServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwtSecurityServiceApplication.class, args);
	}

}
