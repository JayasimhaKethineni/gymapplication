package com.demo.cucumberdemo;

import org.junit.Assert;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DiscountServiceTest {

	DiscountService discountService;
	String percentage = "";

	@Given("^Execute DiscountService$")
	public void execute_DiscountService() throws Throwable {
		discountService = new DiscountService();
	}

	@When("^I enter amount as (\\d+)$")
	public void i_enter_amount_as(int arg1) throws Throwable {
		percentage = discountService.getDiscount(arg1);
	}

	@Then("^I should get ten percent discount$")
	public void i_should_get_ten_percent_discount() throws Throwable {
		Assert.assertEquals("10%", percentage);
	}

	@Then("^I should get fifteen percent discount$")
	public void i_should_get_fifteen_percent_discount() throws Throwable {
		Assert.assertEquals("15%", percentage);
	}

	@Then("I should get no discount")
	public void i_should_get_no_discount() {
		Assert.assertEquals("NA", percentage);
	}
}
