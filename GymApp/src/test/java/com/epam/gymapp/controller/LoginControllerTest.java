package com.epam.gymapp.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.gymapp.model.User;
import com.epam.gymapp.service.impl.LoginServiceImpl;

@WebMvcTest(LoginController.class)
public class LoginControllerTest {

	@Autowired 
	private MockMvc mockMvc; 
	
	@MockBean
	private LoginServiceImpl loginServiceImpl;
	
	@Test
	void authenticateUserTest() throws Exception{
		User user=new User(0,"krisna","mukunda","murari@gmail.com","123",true);
		Mockito.when(loginServiceImpl.authenticate("murari@gmail.com","123")).thenReturn(user);
		mockMvc.perform(post("/gymapp/login")
				.param("username","murari@gmail.com")
				.param("password", "123")
				)
				.andExpect(status().isOk());
	}
	@Test
	void changeLoginTest() throws Exception{
		User user=new User(0,"krisna","mukunda","murari@gmail.com","1234",true);
		Mockito.when(loginServiceImpl.changeLogin("murari@gmail.com","123","1234")).thenReturn(user);
		mockMvc.perform(post("/gymapp/login/update")
				.param("username","murari@gmail.com")
				.param("oldPassword", "123")
				.param("newPassword","1234")
				)
				.andExpect(status().isOk());
	}
}
