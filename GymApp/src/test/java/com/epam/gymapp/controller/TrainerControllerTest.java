package com.epam.gymapp.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.gymapp.dto.request.TrainerDto;
import com.epam.gymapp.dto.request.TrainerRegDto;
import com.epam.gymapp.dto.response.ResponseDto;
import com.epam.gymapp.dto.response.UpdatedTrainerDto;
import com.epam.gymapp.dto.response.ViewTrainerDto;
import com.epam.gymapp.model.Trainer;
import com.epam.gymapp.model.TrainingType;
import com.epam.gymapp.model.User;
import com.epam.gymapp.service.impl.TrainerServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(TrainerController.class)
public class TrainerControllerTest {

	@Autowired 
	private MockMvc mockMvc;
	
	@MockBean
	private TrainerServiceImpl trainerServiceImpl;
	
	@Test
	void addTrainerWithDtoTest() throws Exception{
		TrainingType type=new TrainingType(1, "Strength Training");
		User user=new User(0,"krishna","gopala","murari@gmail.com","123",true);
		Trainer trainer=new Trainer(0,type,user);
		TrainerRegDto trainerRegDto=new TrainerRegDto("krishna","gopala","murari@gmail.com",1);
		ResponseDto responseDto=new ResponseDto("murari@gmail.com", "krishna");
		Mockito.when(trainerServiceImpl.addTrainer(trainerRegDto)).thenReturn(responseDto);
		mockMvc.perform(post("/gymapp/trainers/register")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(trainerRegDto)))
				.andExpect(status().isCreated());
	}
	@Test
	void viewTraineeTest() throws Exception{

		Mockito.when(trainerServiceImpl.getTrainerDtoByUsername("acharya@gmail.com")).thenReturn(any(ViewTrainerDto.class));
		mockMvc.perform(get("/gymapp/trainers/acharya@gmail.com")
				)
				.andExpect(status().isOk());
	}
	@Test
	void updateTraineeTest() throws Exception{
		TrainerDto trainerDto=new TrainerDto("murari@gmail.com", "gopala", "krishna", true,"zumba");
		Mockito.when(trainerServiceImpl.updateTrainer(any(TrainerDto.class))).thenReturn(any(UpdatedTrainerDto.class));
		mockMvc.perform(put("/gymapp/trainers")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(trainerDto)))
				.andExpect(status().isCreated());
	}
	private static String asJsonString(Object object) throws Exception{
		ObjectMapper objectMapper =new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}
}
