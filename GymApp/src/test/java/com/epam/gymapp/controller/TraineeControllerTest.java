package com.epam.gymapp.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.gymapp.dto.request.TraineeDto;
import com.epam.gymapp.dto.request.TraineeRegDto;
import com.epam.gymapp.dto.request.UpdateTraineeList;
import com.epam.gymapp.dto.response.ResponseDto;
import com.epam.gymapp.dto.response.TrainerListDto;
import com.epam.gymapp.dto.response.UpdatedTraineeDto;
import com.epam.gymapp.dto.response.ViewTraineeDto;
import com.epam.gymapp.model.Trainee;
import com.epam.gymapp.model.User;
import com.epam.gymapp.service.impl.TraineeServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;


@WebMvcTest(TraineeController.class)
class TraineeControllerTest {

	@Autowired 
	private MockMvc mockMvc;

	
	@MockBean
	private TraineeServiceImpl traineeServiceImpl;
	
	@Test
	void addTraineeWithDtoTest() throws Exception{
		User user=new User(0,"krishna","gopala","murari","123",true);
		Trainee trainee=new Trainee(0,new Date(),"vrindavan",user);
		TraineeRegDto traineeRegDto=new TraineeRegDto("krishna","gopala","murari@gmail.com",trainee.getDateOfBirth(), trainee.getAddress());

		Mockito.when(traineeServiceImpl.addTrainee(traineeRegDto)).thenReturn(any(ResponseDto.class));
		mockMvc.perform(post("/gymapp/trainees/register")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(traineeRegDto)))
				.andExpect(status().isCreated());
	}
	@Test
	void viewTraineeTest() throws Exception{

		Mockito.when(traineeServiceImpl.getTraineeDtoByUsername("murari@gmail.com")).thenReturn(any(ViewTraineeDto.class));
		mockMvc.perform(get("/gymapp/trainees/murari@gmail.com")
				)
				.andExpect(status().isOk());
	}
	@Test
	void updateTraineeTest() throws Exception{
		TraineeDto traineeDto=new TraineeDto("murari@gmail.com", "gopala", "krishna", new Date(), "vellore", true);
		Mockito.when(traineeServiceImpl.updateTrainee(any(TraineeDto.class))).thenReturn(any(UpdatedTraineeDto.class));
		mockMvc.perform(put("/gymapp/trainees")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(traineeDto)))
				.andExpect(status().isCreated());
	}
	@Test
	void deleteTraineeTest() throws Exception{
		Mockito.doNothing().when(traineeServiceImpl).deleteTrainee("murari@gmail.com");
		mockMvc.perform(delete("/gymapp/trainees/murari@gmail.com"))
				.andExpect(status().isOk());
				
	}
	@Test
	 void updateTrainerListTest() throws Exception{
		List<String> list=new ArrayList<>();
		list.add("acharya@gmail.com");
		UpdateTraineeList updateTraineeList=new UpdateTraineeList("murari@gmail.com", list);
		mockMvc.perform(put("/gymapp/trainees/trainers-list")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(updateTraineeList)))
				.andExpect(status().isCreated());
	}
	@Test
	void notAssignedTrainerListTest() throws Exception{
		List<TrainerListDto> list=new ArrayList<>();
		list.add(TrainerListDto.builder().userName("acharya@gmail.com").firstName("n").lastName("k").specializationName("zumba").build());
		Mockito.when(traineeServiceImpl.notAssignedTrainersToTrainee("murari@gmail.com")).thenReturn(list);
		mockMvc.perform(get("/gymapp/trainees/trainers-not-assigned/murari@gmail.com")
				)
				.andExpect(status().isAccepted());
	}
	
	private static String asJsonString(Object object) throws Exception{
		ObjectMapper objectMapper =new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}
}
