package com.epam.gymapp.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.gymapp.dto.request.TrainingBetweenDates;
import com.epam.gymapp.dto.request.TrainingDto;
import com.epam.gymapp.dto.response.TraineeTrainingDto;
import com.epam.gymapp.dto.response.TrainerTrainingDto;
import com.epam.gymapp.service.impl.TrainingServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(TrainingController.class)
class TrainingControllerTest {

	@Autowired 
	private MockMvc mockMvc; 
	
	@MockBean
	private TrainingServiceImpl trainingServiceImpl;
	
	@Test
	void addTrainingWithDtoTest() throws Exception{
		TrainingDto training=new TrainingDto();
		Mockito.when(trainingServiceImpl.addTraining(training)).thenReturn(training);
		mockMvc.perform(post("/gymapp/trainings")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(training)))
				.andExpect(status().isCreated());
	}
	@Test
	void viewTraineeTrainingListPeriodTest() throws Exception{
		TrainingBetweenDates trainingBetweenDates=new TrainingBetweenDates("murari@gmail.com", new Date(), new Date());
		List<TraineeTrainingDto> list=new ArrayList<>();
		list.add(TraineeTrainingDto.builder().trainerUsername("acharya@gmail.com")
				.trainingDate(new Date()).trainingName("test").build());
		Mockito.when(trainingServiceImpl.viewTraineeTrainingListWithPeriod(trainingBetweenDates)).thenReturn(list);
		mockMvc.perform(post("/gymapp/trainings/trainees-training-list")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(trainingBetweenDates)))
				.andExpect(status().isOk());
	}
	@Test
	void viewTrainerTrainingListPeriodTest() throws Exception{
		TrainingBetweenDates trainingBetweenDates=new TrainingBetweenDates("murari@gmail.com", new Date(), new Date());
		List<TrainerTrainingDto> list=new ArrayList<>();
		list.add(TrainerTrainingDto.builder().traineeUsername("acharya@gmail.com")
				.trainingDate(new Date()).trainingName("test").build());
		Mockito.when(trainingServiceImpl.viewTrainerTrainingListWithPeriod(trainingBetweenDates)).thenReturn(list);
		mockMvc.perform(post("/gymapp/trainings/trainers-training-list")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(trainingBetweenDates)))
				.andExpect(status().isOk());
	}
	private static String asJsonString(Object object) throws Exception{
		ObjectMapper objectMapper =new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}
	
}
