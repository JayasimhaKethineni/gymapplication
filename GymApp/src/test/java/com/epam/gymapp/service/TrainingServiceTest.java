package com.epam.gymapp.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.gymapp.dto.request.TraineeDto;
import com.epam.gymapp.dto.request.TrainingBetweenDates;
import com.epam.gymapp.dto.request.TrainingDto;
import com.epam.gymapp.dto.request.TrainingSummaryDto;
import com.epam.gymapp.dto.response.TraineeTrainingDto;
import com.epam.gymapp.dto.response.TrainerListDto;
import com.epam.gymapp.dto.response.TrainerTrainingDto;
import com.epam.gymapp.dto.response.ViewTraineeDto;
import com.epam.gymapp.exception.GymAppException;
import com.epam.gymapp.kafka.ReportProducer;
import com.epam.gymapp.model.Trainee;
import com.epam.gymapp.model.Trainer;
import com.epam.gymapp.model.Training;
import com.epam.gymapp.model.TrainingType;
import com.epam.gymapp.model.User;
import com.epam.gymapp.repository.TraineeRepository;
import com.epam.gymapp.repository.TrainerRepository;
import com.epam.gymapp.repository.TrainingRepository;
import com.epam.gymapp.repository.TrainingTypeRepository;
import com.epam.gymapp.service.TraineeService;
import com.epam.gymapp.service.TrainerService;
import com.epam.gymapp.service.impl.TrainingServiceImpl;

@ExtendWith(MockitoExtension.class)
public class TrainingServiceTest {
	@InjectMocks
	TrainingServiceImpl trainingServiceImpl;
	
	@Mock
	private TrainingRepository trainingRepository;

	@Mock
	private TraineeRepository traineeRepository;

	@Mock
	private TrainerRepository trainerRepository;

	@Mock
	private TrainingTypeRepository trainingTypeRepository;

	@Mock
	private TraineeService traineeService;

	@Mock
	private TrainerService trainerService;
	
	@Mock
	private ReportProducer reportProducer;

	User user;
	User user1;
	Trainee trainee;
	List<TrainerListDto> trainersDto;
	ViewTraineeDto viewTraineeDto;
	Trainer trainer1;
	TrainingType type;
	TraineeDto traineeDto;
	List<Training> trainings;
	Training training;
	Trainer trainer;
	@BeforeEach
	public void setUp() {
		user=new User(0,"krishna","gopala","murari","123",true);
		user1=new User(0,"rama","narayana","padmanabha","1223",true);
		trainee=new Trainee(0,new Date(),"vrindavan",user);
		trainer1 = new Trainer();
		trainer1.setUser(new User());
        type=new TrainingType(1, "Strength Training");
        trainer1.setSpecializationId(type);
//        trainers.getSpecializationId().setTrainingTypeName("Strength Training");
        List<Trainer> trainers=new ArrayList<>();
        trainers.add(trainer1);
        trainee.setTrainers(trainers);
        
        trainer1.setTrainees(Collections.singletonList(trainee));
        trainings=new ArrayList<>();
        training=new Training(0, trainee, trainer1, "new", type, new Date(), 5);
        trainings.add(training);
        
        trainer=new Trainer(0,type,user1);
        List<Trainee> list=new ArrayList<>();
        list.add(trainee);
//        trainer.setTrainees(Collections.singletonList(trainee));
        trainer.setTrainees(list);
	}
	@Test
	void viewTraineeTrainingListWithPeriodTest() {
		TrainingBetweenDates trainingBetweenDates=new TrainingBetweenDates("murari", new Date(), new Date());
		Mockito.when(traineeService.getTraineeByUsername(trainingBetweenDates.getUsername())).thenReturn(trainee);
		Mockito.when(trainingRepository.findAllTrainingInBetween(trainingBetweenDates.getFrom(),
				trainingBetweenDates.getTo(), trainee)).thenReturn(trainings);
		Mockito.when(trainerRepository.findById(anyInt())).thenReturn(Optional.ofNullable(trainer1));
		List<TraineeTrainingDto> trainingsResult=trainingServiceImpl.viewTraineeTrainingListWithPeriod(trainingBetweenDates);
		Mockito.verify(traineeService).getTraineeByUsername(trainingBetweenDates.getUsername());
		Mockito.verify(trainingRepository).findAllTrainingInBetween(trainingBetweenDates.getFrom(),
				trainingBetweenDates.getTo(), trainee);
		Mockito.verify(trainerRepository).findById(anyInt());
		
	}
	@Test
	void viewTrainerTrainingListWithPeriodTest() {
		TrainingBetweenDates trainingBetweenDates=new TrainingBetweenDates("padmanabha", new Date(), new Date());
		Mockito.when(trainerService.getTrainerByUsername(trainingBetweenDates.getUsername())).thenReturn(trainer);
		Mockito.when(trainingRepository.findAllTrainersTrainingsInBetween(trainingBetweenDates.getFrom(),
				trainingBetweenDates.getTo(), trainer)).thenReturn(trainings);
		Mockito.when(traineeRepository.findById(anyInt())).thenReturn(Optional.ofNullable(trainee));
		List<TrainerTrainingDto> trainingsResult=trainingServiceImpl.viewTrainerTrainingListWithPeriod(trainingBetweenDates);
		Mockito.verify(trainerService).getTrainerByUsername(trainingBetweenDates.getUsername());
		Mockito.verify(trainingRepository).findAllTrainersTrainingsInBetween(trainingBetweenDates.getFrom(),
				trainingBetweenDates.getTo(), trainer);
		Mockito.verify(traineeRepository).findById(anyInt());
		
	}
	@Test
	void addTrainingTest() {
		TrainingDto trainingDto=new TrainingDto(trainee.getUser().getUserName(), trainer.getUser().getUserName(), "new", 1, new Date(), 5);
		Mockito.when(traineeService.getTraineeByUsername(anyString())).thenReturn(trainee);
		Mockito.when(trainerService.getTrainerByUsername(anyString())).thenReturn(trainer);
		Mockito.when(traineeRepository.save(trainee)).thenReturn(trainee);
		Mockito.when(trainerRepository.save(trainer)).thenReturn(trainer);
		Mockito.when(trainingTypeRepository.findById(anyInt())).thenReturn(Optional.ofNullable(type));
		Mockito.when(trainingRepository.save(any(Training.class))).thenReturn(training);
		Mockito.doNothing().when(reportProducer).sendReports(any(TrainingSummaryDto.class));
		TrainingDto result=trainingServiceImpl.addTraining(trainingDto);
		Mockito.verify(traineeService).getTraineeByUsername(anyString());
		Mockito.verify(trainerService).getTrainerByUsername(anyString());
		Mockito.verify(traineeRepository).save(trainee);
		Mockito.verify(trainerRepository).save(trainer);
		Mockito.verify(trainingTypeRepository).findById(anyInt());
		Mockito.verify(trainingRepository).save(any(Training.class));

		assertEquals(trainingDto, result);
	}
}
