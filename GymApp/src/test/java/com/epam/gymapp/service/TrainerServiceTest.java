package com.epam.gymapp.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.epam.gymapp.dto.request.SendMailDto;
import com.epam.gymapp.dto.request.TrainerDto;
import com.epam.gymapp.dto.request.TrainerRegDto;
import com.epam.gymapp.dto.response.ResponseDto;
import com.epam.gymapp.dto.response.TraineeListDto;
import com.epam.gymapp.dto.response.UpdatedTrainerDto;
import com.epam.gymapp.dto.response.ViewTrainerDto;
import com.epam.gymapp.exception.GymAppException;
import com.epam.gymapp.kafka.NotificationProducer;
import com.epam.gymapp.model.Trainee;
import com.epam.gymapp.model.Trainer;
import com.epam.gymapp.model.TrainingType;
import com.epam.gymapp.model.User;
import com.epam.gymapp.repository.TrainerRepository;
import com.epam.gymapp.repository.TrainingTypeRepository;
import com.epam.gymapp.repository.UserRepository;
import com.epam.gymapp.service.impl.TrainerServiceImpl;

@ExtendWith(MockitoExtension.class)
public class TrainerServiceTest {

	@InjectMocks 
	private TrainerServiceImpl trainerServiceImpl;
	
	@Mock 
	private TrainerRepository trainerRepository;
	@Mock 
	private UserRepository userRepository;
	@Mock
	private NotificationProducer notificationProducer;
	@Mock
	private TrainingTypeRepository trainingTypeRepository;
	
	@Mock
	private PasswordEncoder encoder;
	TrainingType type;
	User user;
	Trainer trainer;
	List<TraineeListDto> trainersDto;
	ViewTrainerDto viewTrainerDto;
	Trainee trainee;
	List<Trainee> trainees;
	TrainerDto trainerDto;
	
	@BeforeEach
	void setUp() {
		String username="murari";
		type=new TrainingType(1, "Strength Training");
		user=new User(0,"krishna","gopala","murari","123",true);
		trainer=new Trainer(0,type,user);
		trainee=new Trainee(0,new Date(),"vrindavan",user);
		trainees=new ArrayList<>();
		trainees.add(trainee);
		trainer.setTrainees(trainees);
		trainersDto = trainer.getTrainees().stream()
				.map(trainee -> 
				TraineeListDto.builder().userName(trainee.getUser().getUserName())
				.firstName(trainee.getUser().getFirstName())
				.lastName(trainee.getUser().getLastName()).build()
				)
				.toList();
		viewTrainerDto= ViewTrainerDto.builder().firstName(user.getFirstName())
				.lastName(user.getLastName())
				.isActive(user.getIsActive())
				.specializationName(trainer.getSpecializationId().getTrainingTypeName())
				.traineesList(trainersDto)
				.build();
		trainerDto=new TrainerDto(user.getUserName(), user.getFirstName(), user.getLastName(), user.getIsActive(), trainer.getSpecializationId().getTrainingTypeName());
	}
	
	@Test
	void getTrainerByUsernameTest() {
		TrainingType type=new TrainingType(1, "Strength Training");
		Optional<User> user=Optional.ofNullable(new User(0,"krishna","gopala","murari","123",true));
		Optional<Trainer> trainer=Optional.ofNullable(new Trainer(0,type,user.get()));
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(user);
		Mockito.when(trainerRepository.findByUserId(user.get().getId())).thenReturn(trainer);
		Trainer requiredTrainer=trainerServiceImpl.getTrainerByUsername("murari");
		Mockito.verify(userRepository).findByUserName("murari");
		Mockito.verify(trainerRepository).findByUserId(user.get().getId());
		assertEquals(trainer.get(), requiredTrainer);
	}
	@Test
	void getTrainerByUsernameExceptionTest() {
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.empty());
		
		assertThrows(GymAppException.class,()->trainerServiceImpl.getTrainerByUsername("murari"));
		Mockito.verify(userRepository).findByUserName("murari");
	}
	@Test
	void getTrainerByUsernameException1Test() {
		TrainingType type=new TrainingType(1, "Strength Training");
		Optional<User> user=Optional.ofNullable(new User(0,"krishna","gopala","murari","123",true));
		Optional<Trainer> trainer=Optional.ofNullable(new Trainer(0,type,user.get()));
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(user);
		Mockito.when(trainerRepository.findByUserId(user.get().getId())).thenReturn(Optional.empty());
		assertThrows(GymAppException.class,()->trainerServiceImpl.getTrainerByUsername("murari"));
		Mockito.verify(userRepository).findByUserName("murari");
		Mockito.verify(trainerRepository).findByUserId(user.get().getId());
	}
	
	@Test
	void getTrainerDtoByUsernameTest() {
		String username="murari";
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.ofNullable(user));
		Mockito.when(trainerRepository.findByUserId(user.getId())).thenReturn(Optional.ofNullable(trainer));
						
		ViewTrainerDto required=trainerServiceImpl.getTrainerDtoByUsername(username);
		Mockito.verify(userRepository).findByUserName("murari");
		Mockito.verify(trainerRepository).findByUserId(user.getId());
		assertEquals(viewTrainerDto, required);

	}
	@Test
	void getTrainerDtoByUsernameTest1() {
		String username="murari";
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.empty());
		assertThrows(GymAppException.class,()->trainerServiceImpl.getTrainerDtoByUsername("murari"));
		
		Mockito.verify(userRepository).findByUserName("murari");

	}
	@Test
	void getTrainerDtoByUsernameTest2() {
		String username="murari";
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.ofNullable(user));
		Mockito.when(trainerRepository.findByUserId(user.getId())).thenReturn(Optional.empty());
		assertThrows(GymAppException.class,()->trainerServiceImpl.getTrainerDtoByUsername("murari"));
		
		Mockito.verify(userRepository).findByUserName("murari");
		Mockito.verify(trainerRepository).findByUserId(user.getId());

	}
	@Test
	void updateTrainerTest1() {
		UpdatedTrainerDto updatedTrainerDto = UpdatedTrainerDto.builder()
				.userName(user.getUserName())
				.firstName(user.getFirstName())
				.lastName(user.getLastName())
				.specializationName(trainer.getSpecializationId().getTrainingTypeName())
				.isActive(user.getIsActive())
				.build();
		updatedTrainerDto.setTraineesList(trainersDto);
		
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.ofNullable(user));
		Mockito.when(trainerRepository.findByUserId(user.getId())).thenReturn(Optional.ofNullable(trainer));
		Mockito.when(trainerRepository.save(trainer)).thenReturn(trainer);
		
		UpdatedTrainerDto required=trainerServiceImpl.updateTrainer(trainerDto);
		Mockito.verify(trainerRepository).save(trainer);
		assertEquals(updatedTrainerDto, required);

	}
	@Test
	void addTrainerTest() {
		TrainerRegDto trainerRegDto=new TrainerRegDto("krishna","gopala","murari",trainer.getSpecializationId().getId());

		String password = trainerRegDto.getFirstName() + new Date().hashCode();
		User user= User.builder()
				.firstName(trainerRegDto.getFirstName())
				.lastName(trainerRegDto.getLastName())
				.userName(trainerRegDto.getEmail())
				.password(encoder.encode(password))
				.isActive(true)
				.build();
		Trainer trainer= Trainer.builder()
				.specializationId(type)
				.user(user)
				.build();
		Mockito.when(trainingTypeRepository.findById(trainerRegDto.getSpecializationId())).thenReturn(Optional.ofNullable(type));
		Mockito.when(trainerRepository.save(trainer)).thenReturn(trainer);
		ResponseDto responseDto=ResponseDto.builder().username(trainerRegDto.getEmail()).password(password).build();
		Mockito.doNothing().when(notificationProducer).sendNotification(any(SendMailDto.class));
		ResponseDto result=trainerServiceImpl.addTrainer(trainerRegDto);
		assertEquals(result.getUsername(), responseDto.getUsername());
		
	}
	@Test
	void addTrainerTestException() {
		TrainerRegDto trainerRegDto=new TrainerRegDto("krishna","gopala","murari",trainer.getSpecializationId().getId());

		Mockito.when(trainingTypeRepository.findById(trainerRegDto.getSpecializationId())).thenReturn(Optional.empty());
		assertThrows(GymAppException.class,()->trainerServiceImpl.addTrainer(trainerRegDto));
	}
}
