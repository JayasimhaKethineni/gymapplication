package com.epam.gymapp.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.epam.gymapp.dto.request.SendMailDto;
import com.epam.gymapp.dto.request.TraineeDto;
import com.epam.gymapp.dto.request.TraineeRegDto;
import com.epam.gymapp.dto.request.UpdateTraineeList;
import com.epam.gymapp.dto.response.ResponseDto;
import com.epam.gymapp.dto.response.TrainerListDto;
import com.epam.gymapp.dto.response.UpdatedTraineeDto;
import com.epam.gymapp.dto.response.ViewTraineeDto;
import com.epam.gymapp.exception.GymAppException;
import com.epam.gymapp.kafka.NotificationProducer;
import com.epam.gymapp.model.Trainee;
import com.epam.gymapp.model.Trainer;
import com.epam.gymapp.model.TrainingType;
import com.epam.gymapp.model.User;
import com.epam.gymapp.repository.TraineeRepository;
import com.epam.gymapp.repository.TrainerRepository;
import com.epam.gymapp.repository.UserRepository;
import com.epam.gymapp.service.impl.TraineeServiceImpl;
import com.epam.gymapp.service.impl.TrainerServiceImpl;

@ExtendWith(MockitoExtension.class)
public class TraineeServiceTest {

	@InjectMocks 
	private TraineeServiceImpl traineeServiceImpl;
	
	@Mock 
	private TraineeRepository traineeRepository;
	@Mock
	private TrainerRepository trainerRepository;
	@Mock
	private TrainerServiceImpl trainerServiceImpl;
	@Mock 
	private UserRepository userRepository;
	@Mock
	private NotificationProducer notificationProducer;
	
	@Mock
	private PasswordEncoder encoder;
	
	User user;
	User user1;
	Trainee trainee;
	List<TrainerListDto> trainersDto;
	ViewTraineeDto viewTraineeDto;
	Trainer trainer1;
	TrainingType type;
	TraineeDto traineeDto;
	
	@BeforeEach
	public void setUp() {
		user=new User(0,"krishna","gopala","murari","123",true);
		user1=new User(0,"rama","narayana","padmanabha","1223",true);
		trainee=new Trainee(0,new Date(),"vrindavan",user);
		trainer1 = new Trainer();
		trainer1.setUser(new User());
        type=new TrainingType(1, "Strength Training");
        trainer1.setSpecializationId(type);
//        trainers.getSpecializationId().setTrainingTypeName("Strength Training");
        trainee.setTrainers(Collections.singletonList(trainer1));
        
        trainer1.setTrainees(Collections.singletonList(trainee));
        
		trainersDto = trainee.getTrainers().stream()
				.map(trainer -> 
				TrainerListDto.builder()
				.userName(trainer.getUser().getUserName())
				.firstName(trainer.getUser().getFirstName())
				.lastName(trainer.getUser().getLastName())
				.specializationName(trainer.getSpecializationId().getTrainingTypeName())
				.build()
				)
				.toList();
		viewTraineeDto =ViewTraineeDto.builder()
				.userName(user.getUserName())
				.firstName(user.getFirstName())
				.lastName(user.getLastName())
				.dateOfBirth(trainee.getDateOfBirth())
				.address(trainee.getAddress())
				.isActive(user.getIsActive())
				.trainers(trainersDto)
				.build();
		traineeDto=new TraineeDto(user.getUserName(), user.getFirstName(), user.getLastName(), trainee.getDateOfBirth(), trainee.getAddress(), user.getIsActive());
	}
	
	@Test
	void getTraineeByUsernameTest() {
		Optional<User> user=Optional.ofNullable(new User(0,"krishna","gopala","murari","123",true));
		Optional<Trainee> trainee=Optional.ofNullable(new Trainee(0,new Date(),"vrindavan",user.get()));
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(user);
		Mockito.when(traineeRepository.findByUserId(user.get().getId())).thenReturn(trainee);
		Trainee requiredTrainee=traineeServiceImpl.getTraineeByUsername("murari");
		Mockito.verify(userRepository).findByUserName("murari");
		Mockito.verify(traineeRepository).findByUserId(user.get().getId());
		assertEquals(trainee.get(), requiredTrainee);
	}
	@Test
	void getTraineeByUsernameExceptionTest() {
		Optional<User> user=Optional.ofNullable(new User(0,"krishna","gopala","murari","123",true));
		Optional<Trainee> trainee=Optional.ofNullable(new Trainee(0,new Date(),"vrindavan",user.get()));
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.empty());
//		Mockito.when(traineeRepository.findByUserId(user.get().getId())).thenReturn(trainee);
//		Trainee requiredTrainee=traineeServiceImpl.getTraineeByUsername("murari");
		assertThrows(GymAppException.class,()->traineeServiceImpl.getTraineeByUsername("murari"));
		Mockito.verify(userRepository).findByUserName("murari");
//		Mockito.verify(traineeRepository).findByUserId(user.get().getId());
//		assertEquals(trainee.get(), requiredTrainee);
	}
	@Test
	void getTraineeByUsernameException1Test() {
		Optional<User> user=Optional.ofNullable(new User(0,"krishna","gopala","murari","123",true));
		Optional<Trainee> trainee=Optional.ofNullable(new Trainee(0,new Date(),"vrindavan",user.get()));
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(user);
		Mockito.when(traineeRepository.findByUserId(user.get().getId())).thenReturn(Optional.empty());
		assertThrows(GymAppException.class,()->traineeServiceImpl.getTraineeByUsername("murari"));
		Mockito.verify(userRepository).findByUserName("murari");
		Mockito.verify(traineeRepository).findByUserId(user.get().getId());
	}
	@Test
	void getTraineeDtoByUsernameTest() {
		String username="murari";
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.ofNullable(user));
		Mockito.when(traineeRepository.findByUserId(user.getId())).thenReturn(Optional.ofNullable(trainee));
						
		ViewTraineeDto required=traineeServiceImpl.getTraineeDtoByUsername(username);
		Mockito.verify(userRepository).findByUserName("murari");
		Mockito.verify(traineeRepository).findByUserId(user.getId());
		assertEquals(viewTraineeDto, required);

	}
	@Test
	void getTraineeDtoByUsernameTest1() {
		String username="murari";
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.empty());
//		Mockito.when(traineeRepository.findByUserId(user.getId())).thenReturn(Optional.ofNullable(trainee));
		assertThrows(GymAppException.class,()->traineeServiceImpl.getTraineeDtoByUsername("murari"));
		
//		ViewTraineeDto required=traineeServiceImpl.getTraineeDtoByUsername(username);
		Mockito.verify(userRepository).findByUserName("murari");
//		Mockito.verify(traineeRepository).findByUserId(user.getId());
//		assertEquals(viewTraineeDto, required);

	}
	@Test
	void getTraineeDtoByUsernameTest2() {
		String username="murari";
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.ofNullable(user));
		Mockito.when(traineeRepository.findByUserId(user.getId())).thenReturn(Optional.empty());
		assertThrows(GymAppException.class,()->traineeServiceImpl.getTraineeDtoByUsername("murari"));
		
//		ViewTraineeDto required=traineeServiceImpl.getTraineeDtoByUsername(username);
		Mockito.verify(userRepository).findByUserName("murari");
		Mockito.verify(traineeRepository).findByUserId(user.getId());
//		assertEquals(viewTraineeDto, required);

	}
	
	@Test
	void updateTraineeTest1() {
		UpdatedTraineeDto updatedTraineeDto = UpdatedTraineeDto.builder()
				.userName(user.getUserName())
				.firstName(user.getFirstName())
				.lastName(user.getLastName())
				.address(trainee.getAddress())
				.dateOfBirth(trainee.getDateOfBirth())
				.isActive(user.getIsActive())
				.build();

		updatedTraineeDto.setTrainersList(trainersDto);
		
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.ofNullable(user));
		Mockito.when(traineeRepository.findByUserId(user.getId())).thenReturn(Optional.ofNullable(trainee));
		Mockito.when(traineeRepository.save(trainee)).thenReturn(trainee);
		
		UpdatedTraineeDto required=traineeServiceImpl.updateTrainee(traineeDto);
		Mockito.verify(traineeRepository).save(trainee);
		assertEquals(updatedTraineeDto, required);

	}
	@Test
	void deleteTraineeTest() {
		String username="murari";
		User user=new User(0,"krishna","gopala","murari","123",true);
		Trainee trainee=new Trainee(0,new Date(),"vrindavan",user);
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.ofNullable(user));
		Mockito.when(traineeRepository.findByUserId(user.getId())).thenReturn(Optional.ofNullable(trainee));
		Mockito.doNothing().when(traineeRepository).delete(trainee);
		traineeServiceImpl.deleteTrainee(username);
		Mockito.verify(userRepository).findByUserName("murari");
		Mockito.verify(traineeRepository).findByUserId(user.getId());
		Mockito.verify(traineeRepository).delete(trainee);
		
	}
	@Test
	void updateListTest() {
		Trainee trainee = new Trainee();
		trainee.setUser(user1);
        trainee.getUser().setUserName("murari");


        List<Trainee> trainees=new ArrayList<>();
        trainees.add(trainee);
        Trainer trainer1 = new Trainer();

        trainer1.setUser(user1);
        trainer1.setSpecializationId(type);
        trainer1.setTrainees(trainees);

        Trainer trainer2 = new Trainer();
        trainer2.setUser(user);
        trainer2.setSpecializationId(new TrainingType(2, "Strength Training"));

        List<Trainer> list=new ArrayList<>();
        list.add(trainer1);
        list.add(trainer2);

        UpdateTraineeList updateTraineeList = new UpdateTraineeList();
        updateTraineeList.setUsername("murari");
        updateTraineeList.setTrainers(Arrays.asList("trainer1", "trainer2"));
        trainee.setTrainers(list);

        Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.ofNullable(user));
        Mockito.when(traineeRepository.findByUserId(user.getId())).thenReturn(Optional.ofNullable(trainee));


        Mockito.when(trainerServiceImpl.getTrainerByUsername("trainer1")).thenReturn(trainer1);
        Mockito.when(trainerServiceImpl.getTrainerByUsername("trainer2")).thenReturn(trainer2);
        Mockito.when(traineeRepository.save(trainee)).thenReturn(trainee);


        List<TrainerListDto> result = traineeServiceImpl.updateList(updateTraineeList);


        Mockito.verify(traineeRepository).save(trainee);


        Mockito.verify(trainerServiceImpl).getTrainerByUsername("trainer1");
        Mockito.verify(trainerServiceImpl).getTrainerByUsername("trainer2");

        assertEquals(2, result.size());
		
		
	}
	@Test
	void notAssignedTrainersTest() {
		Trainee trainee = new Trainee();
		trainee.setUser(user1);
        trainee.getUser().setUserName("murari");


        List<Trainee> trainees=new ArrayList<>();
        trainees.add(trainee);
        
		Trainer trainer1 = new Trainer();

        trainer1.setUser(user1);
        trainer1.setSpecializationId(type);
        trainer1.setTrainees(trainees);

        Trainer trainer2 = new Trainer();
        trainer2.setUser(user);
        trainer2.setSpecializationId(new TrainingType(2, "Strength Training"));

        List<Trainer> list=new ArrayList<>();
        list.add(trainer1);
        list.add(trainer2);
        trainee.setTrainers(Arrays.asList(trainer1));
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.ofNullable(user));
        Mockito.when(traineeRepository.findByUserId(user.getId())).thenReturn(Optional.ofNullable(trainee));
        Mockito.when(trainerRepository.findAll()).thenReturn(list);
        List<TrainerListDto> result = traineeServiceImpl.notAssignedTrainersToTrainee("murari");
        Mockito.verify(userRepository).findByUserName("murari");
		Mockito.verify(traineeRepository).findByUserId(user.getId());
		Mockito.verify(trainerRepository).findAll();

        assertEquals(1, result.size());

	}
	@Test
	void addTraineeTest() {
		TraineeRegDto traineeRegDto=new TraineeRegDto("krishna","gopala","murari",trainee.getDateOfBirth(), trainee.getAddress());

		String password = traineeRegDto.getFirstName() + new Date().hashCode();
		User user= User.builder()
				.firstName(traineeRegDto.getFirstName())
				.lastName(traineeRegDto.getLastName())
				.userName(traineeRegDto.getEmail())
				.password(encoder.encode(password))
				.isActive(true)
				.build();
		Trainee trainee= Trainee.builder()
				.address(traineeRegDto.getAddress())
				.dateOfBirth(traineeRegDto.getDateOfBirth())
				.user(user)
				.build();
		Mockito.when(traineeRepository.save(trainee)).thenReturn(trainee);
		ResponseDto responseDto=ResponseDto.builder().username(traineeRegDto.getEmail()).password(password).build();
		Mockito.doNothing().when(notificationProducer).sendNotification(any(SendMailDto.class));
		ResponseDto result=traineeServiceImpl.addTrainee(traineeRegDto);
		assertEquals(result.getUsername(), responseDto.getUsername());
		
	}
}
