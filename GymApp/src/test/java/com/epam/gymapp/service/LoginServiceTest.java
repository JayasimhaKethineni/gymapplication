package com.epam.gymapp.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.gymapp.exception.GymAppException;
import com.epam.gymapp.model.User;
import com.epam.gymapp.repository.UserRepository;
import com.epam.gymapp.service.impl.LoginServiceImpl;

@ExtendWith(MockitoExtension.class)
public class LoginServiceTest {

	@InjectMocks 
	private LoginServiceImpl loginServiceImpl;
	
	@Mock 
	private UserRepository userRepository;
	
	List<User> users;
	@BeforeEach
	public void setUp() {
		users=new ArrayList<>(Arrays.asList(new User(0,"krishna","gopala","murari","123",true), 
				new User(0,"rama","narayana","padmanabha","1223",true)));
	}
	@Test 
	void authenticateTest() {
		Optional<User> user=Optional.ofNullable(new User(0,"krishna","gopala","murari","123",true));
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(user);

		User requiredUser=loginServiceImpl.authenticate("murari","123");
		Mockito.verify(userRepository).findByUserName("murari");

		assertEquals(user.get(), requiredUser);
		
	} 
	@Test
	void authenticateTestException1() {
		Optional<User> user=Optional.ofNullable(new User(0,"krishna","gopala","murari","123",true));

		Mockito.when(userRepository.findByUserName("murari")).thenReturn(user);

		assertThrows(GymAppException.class,()->loginServiceImpl.authenticate("murari","kris12345"));
		Mockito.verify(userRepository).findByUserName("murari");
		
		
	}
	@Test
	void authenticateTestException() {
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.empty());

		assertThrows(GymAppException.class,()->loginServiceImpl.authenticate("murari","123"));
		Mockito.verify(userRepository).findByUserName("murari");
		
		
	}
	@Test 
	void changeLoginTest() {
		Optional<User> user=Optional.ofNullable(new User(0,"krishna","gopala","murari","123",true));
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(user);

		User requiredUser=loginServiceImpl.changeLogin("murari","123","1234");
		Mockito.verify(userRepository).findByUserName("murari");

		assertEquals("1234", requiredUser.getPassword());
		
	} 
	@Test
	void changeLoginTestException1() {
		Optional<User> user=Optional.ofNullable(new User(0,"krishna","gopala","murari","123",true));

		Mockito.when(userRepository.findByUserName("murari")).thenReturn(user);

		assertThrows(GymAppException.class,()->loginServiceImpl.changeLogin("murari","kris12345","1234"));
		Mockito.verify(userRepository).findByUserName("murari");
		 
		
	}
	@Test
	void changeLoginTestException() {
		Mockito.when(userRepository.findByUserName("murari")).thenReturn(Optional.empty());

		assertThrows(GymAppException.class,()->loginServiceImpl.changeLogin("murari","123","1234"));
		Mockito.verify(userRepository).findByUserName("murari");
		
		
	}
	
}
