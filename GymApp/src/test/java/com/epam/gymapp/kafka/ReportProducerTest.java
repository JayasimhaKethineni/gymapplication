package com.epam.gymapp.kafka;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;

import com.epam.gymapp.dto.request.TrainingSummaryDto;

@ExtendWith(MockitoExtension.class)
public class ReportProducerTest {

	@Mock
    private KafkaTemplate<String, TrainingSummaryDto> kafkaTemplate;

    @InjectMocks
    private ReportProducer reportProducer;
    
    @Test
    void testSendReport() {
        TrainingSummaryDto trainingSummaryDto=new TrainingSummaryDto();

        reportProducer.sendReports(trainingSummaryDto);

        verify(kafkaTemplate, times(1)).send(eq("reports"), eq(trainingSummaryDto));
    }
}
