package com.epam.gymapp.kafka;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;

import com.epam.gymapp.dto.request.SendMailDto;

@ExtendWith(MockitoExtension.class)
public class NotificationProducerTest {

	@Mock
    private KafkaTemplate<String, SendMailDto> kafkaTemplate;

    @InjectMocks
    private NotificationProducer notificationProducer;
    
    @Test
    void testSendNotification() {
        SendMailDto sendMailDto = new SendMailDto();
        sendMailDto.setToMail("recipient@example.com");
        sendMailDto.setSubject("Test Subject");
        sendMailDto.setBody("Test Body");
        sendMailDto.setCcMail("cc@example.com");

        notificationProducer.sendNotification(sendMailDto);

        verify(kafkaTemplate, times(1)).send(eq("notifications"), eq(sendMailDto));
    }
}
