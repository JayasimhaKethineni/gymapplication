package com.epam.gymapp.dto.request;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TraineeDto {
             
	@NotBlank(message = "please enter valid username(email) !!")
	@Email(message = "please enter valid email!!")
	private String userName;
	@NotBlank(message = "please enter valid firstName !!")
	private String firstName;
	@NotBlank(message = "please enter valid lastname !!")
	private String lastName;
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date dateOfBirth;
	@NotBlank(message = "please enter valid address !!")
	private String address;
	private boolean isActive;
	
}
