package com.epam.gymapp.dto.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ViewTrainerDto {
	
	private String firstName;
	private String lastName;
	private boolean isActive;
	private String specializationName;
	private List<TraineeListDto> traineesList;
}
