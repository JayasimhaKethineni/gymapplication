package com.epam.gymapp.dto.request;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TrainingBetweenDates {

	@NotBlank(message = "please enter valid username(email) !!!")
	@Email(message = "please enter valid email !!!")
	private String username;
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date from;
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date to;
}
