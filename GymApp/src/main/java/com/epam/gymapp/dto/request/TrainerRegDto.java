package com.epam.gymapp.dto.request;


import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TrainerRegDto {

	@NotBlank(message = "please enter valid firstname !!!")
	private String firstName;
	@NotBlank(message = "please enter valid lastName !!!")
	private String lastName;
	@NotBlank(message = "please enter valid email !!!")
	@Email(message = "please enter valid email !!")
	private String email;
	@Min(value = 1,message = "please enter value greater than or equal to 1 !!!")
	@Max(value=5,message = "please enter value less than or equal to 5 !!!")
	private int specializationId;
}
