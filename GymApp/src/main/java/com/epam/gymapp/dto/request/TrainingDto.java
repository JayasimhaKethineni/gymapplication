package com.epam.gymapp.dto.request;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TrainingDto {

	private String traineeUserName;
	
	private String trainerUserName;
	
	private String trainingName;
	
	private int trainingTypeId;
	
	private Date trainingDate;
	
	private int trainingDuration;
}
