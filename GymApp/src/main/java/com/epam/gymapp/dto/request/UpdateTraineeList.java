package com.epam.gymapp.dto.request;

import java.util.List;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UpdateTraineeList {

	@NotBlank(message = "please enter valid username(email) !!")
	private String username;
	@NotEmpty(message = "please don't leave empty!!")
	private List<String> trainers;
}
