package com.epam.gymapp.dto.response;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ViewTraineeDto {

	private String userName;
	private String firstName;
	private String lastName;
	private Date dateOfBirth;
	private String address;
	private boolean isActive;
	private List<TrainerListDto> trainers;
}
