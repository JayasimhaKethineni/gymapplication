package com.epam.gymapp.dto.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TrainerDto {

	@NotBlank(message = "please enter valid username(email) !!")
	@Email(message = "please enter email!!")
	private String userName;
	@NotBlank(message = "please enter valid firstName !!")
	private String firstName;
	@NotBlank(message = "please enter valid lastName !!")
	private String lastName;

	private boolean isActive;
	@NotBlank(message = "please enter valid specializationName !!")
	private String specializationName;
}
