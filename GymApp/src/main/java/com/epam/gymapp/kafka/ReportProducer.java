package com.epam.gymapp.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.epam.gymapp.dto.request.TrainingSummaryDto;

import jakarta.annotation.PreDestroy;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ReportProducer {

	@Autowired
	private KafkaTemplate<String, TrainingSummaryDto> kafkaTemplate;
	
	public void sendReports(TrainingSummaryDto trainingSummaryDto) {
		log.info("msg sent by kafka : " + trainingSummaryDto);
		kafkaTemplate.send("reports", trainingSummaryDto);
	}
	
	@PreDestroy
	public void close() {
		if(kafkaTemplate!=null) {
		kafkaTemplate.destroy();
		}
	}
}
