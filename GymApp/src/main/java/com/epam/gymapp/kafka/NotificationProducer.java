package com.epam.gymapp.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.epam.gymapp.dto.request.SendMailDto;

import jakarta.annotation.PreDestroy;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class NotificationProducer {
	@Autowired
	private KafkaTemplate<String, SendMailDto> kafkaTemplate;
	
	public void sendNotification(SendMailDto sendMailDto) {
		log.info("email sent by kafka : " + sendMailDto);
		kafkaTemplate.send("notifications", sendMailDto);
	}
	
	@PreDestroy
	public void close() {
		if(kafkaTemplate!=null) {
		kafkaTemplate.destroy();
		}
	}
	
}
