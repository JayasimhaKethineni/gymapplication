package com.epam.gymapp.exception;

public class GymAppException extends RuntimeException{

	public GymAppException() {
		super();
	}

	public GymAppException(String message) {
		super(message);
	}
	
	

}
