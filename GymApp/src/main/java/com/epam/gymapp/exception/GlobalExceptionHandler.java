package com.epam.gymapp.exception;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

	@ExceptionHandler(GymAppException.class)
	@ResponseStatus(value = HttpStatus.OK)
	public ExceptionResponse exceptionHandler(GymAppException e, WebRequest re) {
		log.info("GymAppException occured!!!");
		return new ExceptionResponse(new Date().toString(), HttpStatus.OK.toString(), e.getMessage(), re.getContextPath());
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ExceptionResponse dtoValidation(MethodArgumentNotValidException e, WebRequest re) {
		List<String> errors = new ArrayList<>();
		log.info("MethodArgumentNotValidException occured!!!");

		e.getAllErrors().forEach(error -> errors.add(error.getDefaultMessage()));
		return new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(), errors.toString(),
				re.getContextPath());

	}
	
	@ExceptionHandler(RuntimeException.class)
	public ExceptionResponse dtoValidation(RuntimeException e, WebRequest re) {
		log.info("RuntimeException occured!!!");

		return new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(), e.getMessage(),
				re.getContextPath());
	}


}
