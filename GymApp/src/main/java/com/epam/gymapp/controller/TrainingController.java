package com.epam.gymapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.gymapp.dto.request.TrainingBetweenDates;
import com.epam.gymapp.dto.request.TrainingDto;
import com.epam.gymapp.dto.response.TraineeTrainingDto;
import com.epam.gymapp.dto.response.TrainerTrainingDto;
import com.epam.gymapp.service.TrainingService;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("gymapp/trainings")
@Slf4j
public class TrainingController {

	@Autowired
	private TrainingService trainingService;

	@PostMapping
	public ResponseEntity<TrainingDto> addTraining(@RequestBody @Valid TrainingDto trainingDto) {
		log.info("invoked addTraining() controller...");
		return new ResponseEntity<>(trainingService.addTraining(trainingDto), HttpStatus.CREATED);
	}

//	@GetMapping("/{username}")
//	public ResponseEntity<List<TrainerTrainingDto>> viewTrainerTrainingList(@PathVariable String username) {
//		log.info("invoked viewTrainerTrainingList() controller...");
//		return new ResponseEntity<>(trainingService.viewTrainerTrainingList(username), HttpStatus.CREATED);
//	}
//
//	@GetMapping("trainees/{username}")
//	public ResponseEntity<List<TraineeTrainingDto>> viewTraineeTrainingList(@PathVariable String username) {
//		log.info("invoked viewTraineeTrainingList() controller...");
//		return new ResponseEntity<>(trainingService.viewTraineeTrainingList(username), HttpStatus.CREATED);
//	}

	@PostMapping("/trainees-training-list")
	public ResponseEntity<List<TraineeTrainingDto>> viewTraineeTrainingListPeriod(
			@RequestBody @Valid TrainingBetweenDates trainingBetweenDates) {
		log.info("invoked viewTraineeTrainingListPeriod() controller...");
		return new ResponseEntity<>(trainingService.viewTraineeTrainingListWithPeriod(trainingBetweenDates),
				HttpStatus.OK);
	}

	@PostMapping("/trainers-training-list")
	public ResponseEntity<List<TrainerTrainingDto>> viewTrainerTrainingListPeriod(
			@RequestBody @Valid TrainingBetweenDates trainingBetweenDates) {
		log.info("invoked viewTrainerTrainingListPeriod() controller...");
		return new ResponseEntity<>(trainingService.viewTrainerTrainingListWithPeriod(trainingBetweenDates),
				HttpStatus.OK);
	}
}
