package com.epam.gymapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.gymapp.dto.request.TraineeDto;
import com.epam.gymapp.dto.request.TraineeRegDto;
import com.epam.gymapp.dto.request.UpdateTraineeList;
import com.epam.gymapp.dto.response.ResponseDto;
import com.epam.gymapp.dto.response.TrainerListDto;
import com.epam.gymapp.dto.response.UpdatedTraineeDto;
import com.epam.gymapp.dto.response.ViewTraineeDto;
import com.epam.gymapp.service.TraineeService;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("gymapp/trainees")
@Slf4j
public class TraineeController {

	@Autowired
	private TraineeService traineeService;

	@PostMapping("/register")
	public ResponseEntity<ResponseDto> addTraineeDto(@RequestBody @Valid TraineeRegDto traineeRegDto) {
		log.info("invoked addTraineeDto() controller...");
		return new ResponseEntity<>(traineeService.addTrainee(traineeRegDto), HttpStatus.CREATED);
	}

	@GetMapping("/{username}")
	public ResponseEntity<ViewTraineeDto> viewTrainee(@PathVariable String username) {
		log.info("invoked viewTrainee() controller...");
		return new ResponseEntity<>(traineeService.getTraineeDtoByUsername(username), HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<UpdatedTraineeDto> updateTrainee(@RequestBody @Valid TraineeDto traineeDto) {
		log.info("invoked updateTrainee() controller...");
		return new ResponseEntity<>(traineeService.updateTrainee(traineeDto), HttpStatus.CREATED);
	}

	@DeleteMapping("/{username}")
	public ResponseEntity<Void> deleteTrainee(@PathVariable String username) {
		log.info("invoked deleteTrainee() controller...");

		traineeService.deleteTrainee(username);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PutMapping("/trainers-list")
	public ResponseEntity<List<TrainerListDto>> updateTrainerList(
			@RequestBody @Valid UpdateTraineeList updateTraineeList) {
		log.info("invoked updateTrainerList() controller...");
		return new ResponseEntity<>(traineeService.updateList(updateTraineeList), HttpStatus.CREATED);
	}

	@GetMapping("/trainers-not-assigned/{username}")
	public ResponseEntity<List<TrainerListDto>> notAssignedTrainerList(@PathVariable String username) {
		log.info("invoked notAssignedTrainerList() controller...");
		return new ResponseEntity<>(traineeService.notAssignedTrainersToTrainee(username), HttpStatus.ACCEPTED);
	}

}
