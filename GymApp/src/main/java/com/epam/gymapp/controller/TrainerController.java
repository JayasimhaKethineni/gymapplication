package com.epam.gymapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.gymapp.dto.request.TrainerDto;
import com.epam.gymapp.dto.request.TrainerRegDto;
import com.epam.gymapp.dto.response.ResponseDto;
import com.epam.gymapp.dto.response.UpdatedTrainerDto;
import com.epam.gymapp.dto.response.ViewTrainerDto;
import com.epam.gymapp.service.TrainerService;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("gymapp/trainers")
@Slf4j
public class TrainerController {

	@Autowired
	private TrainerService trainerService;

	@PostMapping("/register")
	public ResponseEntity<ResponseDto> addTrainerDto(@RequestBody @Valid TrainerRegDto trainerRegDto) {
		log.info("invoked addTrainerDto() controller...");
		return new ResponseEntity<>(trainerService.addTrainer(trainerRegDto), HttpStatus.CREATED);
	}

	@PutMapping
	public ResponseEntity<UpdatedTrainerDto> updateTrainer(@RequestBody @Valid TrainerDto trainerDto) {
		log.info("invoked updateTrainer() controller...");
		return new ResponseEntity<>(trainerService.updateTrainer(trainerDto), HttpStatus.CREATED);
	}
	@GetMapping("/{username}")
	public ResponseEntity<ViewTrainerDto> viewTrainer(@PathVariable String username) {
		log.info("invoked viewTrainer() controller...");
		return new ResponseEntity<>(trainerService.getTrainerDtoByUsername(username), HttpStatus.OK);
	}
}
