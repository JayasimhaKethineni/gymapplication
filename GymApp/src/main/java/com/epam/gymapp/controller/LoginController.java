package com.epam.gymapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.gymapp.model.User;
import com.epam.gymapp.service.LoginService;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("gymapp/login")
@Slf4j
public class LoginController {

	@Autowired
	private LoginService loginService;
	
	@PostMapping
	public ResponseEntity<User> authenticateUser(@RequestParam @Valid @NotBlank(message = "username must not be blank!!") String username,@RequestParam @Valid @NotBlank(message = "password must not be blank!!") String password){
		log.info("invoked authenticateUser() controller..");
		return new ResponseEntity<>(loginService.authenticate(username, password),HttpStatus.OK);
	}
	@PostMapping("/update")
	public ResponseEntity<User> changeLoginCredentials(@RequestParam @Valid @NotBlank(message = "username must not be blank!!") String username,@RequestParam @Valid @NotBlank(message = "oldPassword must not be blank!!") String oldPassword,@RequestParam @Valid @NotBlank(message = "newPassword must not be blank!!") String newPassword){
		log.info("invoked changeLoginCredentials() controller...");
		return new ResponseEntity<>(loginService.changeLogin(username, oldPassword,newPassword),HttpStatus.OK);
	}
}
