package com.epam.gymapp.model;

import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Training {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne
	@JoinColumn(name = "trainee_id")
	private Trainee trainee;

	@ManyToOne
	@JoinColumn(name = "trainer_id")
	private Trainer trainer;

	private String trainingName;

	@ManyToOne
	@JoinColumn(name = "training_type_id")
	private TrainingType trainingType;

	private Date trainingDate;

	private int trainingDuration;

}
