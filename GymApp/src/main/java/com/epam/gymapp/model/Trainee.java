package com.epam.gymapp.model;

import java.util.Date;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Trainee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	Date dateOfBirth;
	String address;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id")
	private User user;

	@ManyToMany
	@JoinTable(name = "Trainee2Trainer", joinColumns = @JoinColumn(name = "trainee_id"), inverseJoinColumns = @JoinColumn(name = "trainer_id"))
	private List<Trainer> trainers;

	public Trainee(int id, Date dateOfBirth, String address, User user) {
		super();
		this.id = id;
		this.dateOfBirth = dateOfBirth;
		this.address = address;
		this.user = user;
	}

	@Override
	public String toString() {
		return "Trainee [id=" + id + ", dateOfBirth=" + dateOfBirth + ", address=" + address + ", user=" + user + "]";
	}

}
