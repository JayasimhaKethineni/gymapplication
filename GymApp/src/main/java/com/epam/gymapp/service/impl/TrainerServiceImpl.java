package com.epam.gymapp.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.epam.gymapp.dto.request.SendMailDto;
import com.epam.gymapp.dto.request.TrainerDto;
import com.epam.gymapp.dto.request.TrainerRegDto;
import com.epam.gymapp.dto.response.ResponseDto;
import com.epam.gymapp.dto.response.TraineeListDto;
import com.epam.gymapp.dto.response.UpdatedTrainerDto;
import com.epam.gymapp.dto.response.ViewTrainerDto;
import com.epam.gymapp.exception.GymAppException;
import com.epam.gymapp.kafka.NotificationProducer;
import com.epam.gymapp.model.Trainer;
import com.epam.gymapp.model.TrainingType;
import com.epam.gymapp.model.User;
import com.epam.gymapp.repository.TrainerRepository;
import com.epam.gymapp.repository.TrainingTypeRepository;
import com.epam.gymapp.repository.UserRepository;
import com.epam.gymapp.service.TrainerService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TrainerServiceImpl implements TrainerService {

	@Autowired
	private TrainerRepository trainerRepository;

	@Autowired
	private TrainingTypeRepository trainingTypeRepository;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder encoder;

	
	@Autowired
	private NotificationProducer notificationProducer;

	public ResponseDto addTrainer(TrainerRegDto trainerRegDto) {
		log.info("invoked addTrainer() method with input:",trainerRegDto);
		String password = trainerRegDto.getFirstName() + new Date().hashCode();
		User user= User.builder()
				.firstName(trainerRegDto.getFirstName())
				.lastName(trainerRegDto.getLastName())
				.userName(trainerRegDto.getEmail())
				.password(encoder.encode(password))
				.isActive(true)
				.build();
		TrainingType trainingType = trainingTypeRepository.findById(trainerRegDto.getSpecializationId())
				.orElseThrow(() -> new GymAppException("enter valid training type id!!"));
		Trainer trainer= Trainer.builder().specializationId(trainingType).user(user).build();
		trainerRepository.save(trainer);
		String body="welcome to gym application!! you are registered as trainer \n username: "+trainerRegDto.getEmail()+"\n password: "+password+"\n thank you";
		String subject="Registration Confirmation - GYM Application";
		SendMailDto sendMailDto=new SendMailDto(trainerRegDto.getEmail(), "jayk070707@gmail.com", subject, body);
		
		notificationProducer.sendNotification(sendMailDto);
		return ResponseDto.builder().username(trainerRegDto.getEmail()).password(password).build();
	}

	public Trainer getTrainerByUsername(String username) {
		log.info("invoked getTrainerByUsername() method with username:",username);

		User user = userRepository.findByUserName(username)
				.orElseThrow(() -> new GymAppException("enter valid username!!"));
		return  trainerRepository.findByUserId(user.getId())
				.orElseThrow(() -> new GymAppException("verify the user properly!!"));
	}

	public ViewTrainerDto getTrainerDtoByUsername(String username) {
		log.info("invoked getTrainerDtoByUsername() method with username:",username);

		User user = userRepository.findByUserName(username)
				.orElseThrow(() -> new GymAppException("enter valid username!!"));
		Trainer trainer = trainerRepository.findByUserId(user.getId())
				.orElseThrow(() -> new GymAppException("verify the user properly!!"));
		List<TraineeListDto> trainersDto = trainer.getTrainees().stream()
				.map(trainee -> 
				TraineeListDto.builder().userName(trainee.getUser().getUserName())
				.firstName(trainee.getUser().getFirstName())
				.lastName(trainee.getUser().getLastName()).build()
				)
				.toList();
		return ViewTrainerDto.builder().firstName(user.getFirstName())
				.lastName(user.getLastName())
				.isActive(user.getIsActive())
				.specializationName(trainer.getSpecializationId().getTrainingTypeName())
				.traineesList(trainersDto)
				.build();
	}
	public UpdatedTrainerDto updateTrainer(TrainerDto trainerDto) {
		log.info("invoked updateTrainer() method with input:",trainerDto);

		Trainer trainer = getTrainerByUsername(trainerDto.getUserName());
		trainer.getUser().setFirstName(trainerDto.getFirstName());
		trainer.getUser().setLastName(trainerDto.getLastName());
		trainer.getUser().setIsActive(trainerDto.isActive());
		trainerRepository.save(trainer);
//		UpdatedTrainerDto updatedTrainerDto = mapper.map(trainerDto, UpdatedTrainerDto.class);
		UpdatedTrainerDto updatedTrainerDto = UpdatedTrainerDto.builder()
				.userName(trainerDto.getUserName())
				.firstName(trainerDto.getFirstName())
				.lastName(trainerDto.getLastName())
				.specializationName(trainerDto.getSpecializationName())
				.isActive(trainerDto.isActive())
				.build();
		updatedTrainerDto.setTraineesList(
				trainer.getTrainees().stream().map(trainee -> 
				TraineeListDto.builder()
				.userName(trainee.getUser().getUserName())
				.firstName(trainee.getUser().getFirstName())
				.lastName(trainee.getUser().getLastName())
				.build()
				).toList());
		notificationProducer.sendNotification(SendMailDto.builder().toMail(updatedTrainerDto.getUserName())
				.ccMail("jj@gmail.com").body("your profile is updated!!").subject("Update alert!!!").build());
				
		
		return updatedTrainerDto;
	}

}
