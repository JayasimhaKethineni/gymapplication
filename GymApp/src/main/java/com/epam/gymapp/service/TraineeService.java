package com.epam.gymapp.service;

import java.util.List;

import com.epam.gymapp.dto.request.TraineeDto;
import com.epam.gymapp.dto.request.TraineeRegDto;
import com.epam.gymapp.dto.request.UpdateTraineeList;
import com.epam.gymapp.dto.response.ResponseDto;
import com.epam.gymapp.dto.response.TrainerListDto;
import com.epam.gymapp.dto.response.UpdatedTraineeDto;
import com.epam.gymapp.dto.response.ViewTraineeDto;
import com.epam.gymapp.model.Trainee;

public interface TraineeService {


	public ResponseDto addTrainee(TraineeRegDto traineeRegDto);

	public Trainee getTraineeByUsername(String username);

	public ViewTraineeDto getTraineeDtoByUsername(String username);

	public UpdatedTraineeDto updateTrainee(TraineeDto traineeDto);

	public void deleteTrainee(String username);

	public List<TrainerListDto> updateList(UpdateTraineeList updateTraineeList);

	public List<TrainerListDto> notAssignedTrainersToTrainee(String username);

}
