package com.epam.gymapp.service;

import com.epam.gymapp.dto.request.TrainerDto;
import com.epam.gymapp.dto.request.TrainerRegDto;
import com.epam.gymapp.dto.response.ResponseDto;
import com.epam.gymapp.dto.response.UpdatedTrainerDto;
import com.epam.gymapp.dto.response.ViewTrainerDto;
import com.epam.gymapp.model.Trainer;

public interface TrainerService {


	public ResponseDto addTrainer(TrainerRegDto trainerRegDto);

	public Trainer getTrainerByUsername(String username);

	public ViewTrainerDto getTrainerDtoByUsername(String username);
	
	public UpdatedTrainerDto updateTrainer(TrainerDto trainerDto);

}
