package com.epam.gymapp.service;

import java.util.List;

import com.epam.gymapp.dto.request.TrainingBetweenDates;
import com.epam.gymapp.dto.request.TrainingDto;
import com.epam.gymapp.dto.response.TraineeTrainingDto;
import com.epam.gymapp.dto.response.TrainerTrainingDto;

public interface TrainingService {


	public TrainingDto addTraining(TrainingDto trainingDto);

//	public List<TrainerTrainingDto> viewTrainerTrainingList(String username);
//
//	public List<TraineeTrainingDto> viewTraineeTrainingList(String username);

	public List<TraineeTrainingDto> viewTraineeTrainingListWithPeriod(TrainingBetweenDates trainingBetweenDates);

	public List<TrainerTrainingDto> viewTrainerTrainingListWithPeriod(TrainingBetweenDates trainingBetweenDates);
}
