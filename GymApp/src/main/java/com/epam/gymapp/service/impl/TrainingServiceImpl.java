package com.epam.gymapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.gymapp.dto.request.TrainingBetweenDates;
import com.epam.gymapp.dto.request.TrainingDto;
import com.epam.gymapp.dto.request.TrainingSummaryDto;
import com.epam.gymapp.dto.response.TraineeTrainingDto;
import com.epam.gymapp.dto.response.TrainerTrainingDto;
import com.epam.gymapp.exception.GymAppException;
import com.epam.gymapp.kafka.ReportProducer;
import com.epam.gymapp.model.Trainee;
import com.epam.gymapp.model.Trainer;
import com.epam.gymapp.model.Training;
import com.epam.gymapp.model.TrainingType;
import com.epam.gymapp.repository.TraineeRepository;
import com.epam.gymapp.repository.TrainerRepository;
import com.epam.gymapp.repository.TrainingRepository;
import com.epam.gymapp.repository.TrainingTypeRepository;
import com.epam.gymapp.service.TraineeService;
import com.epam.gymapp.service.TrainerService;
import com.epam.gymapp.service.TrainingService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TrainingServiceImpl implements TrainingService {

	@Autowired
	private TrainingRepository trainingRepository;

	@Autowired
	private TraineeRepository traineeRepository;

	@Autowired
	private TrainerRepository trainerRepository;

	@Autowired
	private TrainingTypeRepository trainingTypeRepository;

	@Autowired
	private TraineeService traineeService;

	@Autowired
	private TrainerService trainerService;
	
	@Autowired
	private ReportProducer reportProducer;

	public TrainingDto addTraining(TrainingDto trainingDto) {
		log.info("invoked addTraining() with dto: ",trainingDto);
		Trainee trainee = traineeService.getTraineeByUsername(trainingDto.getTraineeUserName());
		Trainer trainer = trainerService.getTrainerByUsername(trainingDto.getTrainerUserName());
		trainee.getTrainers().add(trainer);
		trainer.getTrainees().add(trainee);
		traineeRepository.save(trainee);
		trainerRepository.save(trainer);
		TrainingType trainingType = trainingTypeRepository.findById(trainingDto.getTrainingTypeId())
				.orElseThrow(() -> new GymAppException("enter valid trainingTypeId !!!"));
		
		trainingRepository.save(Training.builder().trainee(trainee).trainer(trainer)
				.trainingName(trainingDto.getTrainingName())
				.trainingType(trainingType)
				.trainingDate(trainingDto.getTrainingDate())
				.trainingDuration(trainingDto.getTrainingDuration())
				.build());
//		trainingRepository.save(Training.builder().trainee(trainee).trainer(trainer)
//				.trainingName(trainingDto.getTrainingName())
//				.trainingType(trainer.getSpecializationId())
//				.trainingDate(trainingDto.getTrainingDate())
//				.trainingDuration(trainingDto.getTrainingDuration())
//				.build());
		
		reportProducer.sendReports(TrainingSummaryDto.builder()
				.trainingName(trainingDto.getTrainingName())
				.trainingDate(trainingDto.getTrainingDate())
				.trainingDuration(trainingDto.getTrainingDuration())
				.trainerUsername(trainer.getUser().getUserName())
				.trainerFirstName(trainer.getUser().getFirstName())
				.trainerLastName(trainer.getUser().getLastName())
				.build());
		return trainingDto;
	}

//	public List<TrainerTrainingDto> viewTrainerTrainingList(String username) {
//		log.info("invoked viewTrainerTrainingList() with username: ",username);
//
//		Trainer trainer = trainerService.getTrainerByUsername(username);
//		List<Training> list = trainingRepository.getAllByTrainerId(trainer.getId());
//		return list.stream()
//				.map(training -> 
//				TrainerTrainingDto.builder()
//				.trainingName(training.getTrainingName())
//				.trainingTypeId(training.getTrainingType().getId())
//				.trainingDate(training.getTrainingDate())
//				.trainingDuration(training.getTrainingDuration())
//				.traineeUsername(traineeRepository.findById(training.getTrainee().getId()).get().getUser().getFirstName())
//				.build()
//				)
//				.toList();
//	}
//
//	public List<TraineeTrainingDto> viewTraineeTrainingList(String username) {
//		log.info("invoked viewTraineeTrainingList() with username: ",username);
//		Trainee trainee = traineeService.getTraineeByUsername(username);
//		List<Training> list = trainingRepository.getAllByTrainerId(trainee.getId());
//		return list.stream()
//				.map(training ->
//				TraineeTrainingDto.builder()
//				.trainingName(training.getTrainingName())
//				.trainingTypeId(training.getTrainingType().getId())
//				.trainingDate(training.getTrainingDate())
//				.trainingDuration(training.getTrainingDuration())
//				.trainerUsername(trainerRepository.findById(training.getTrainee().getId()).get().getUser().getFirstName())
//				.build()
//				)
//				.toList();
//	}

	public List<TraineeTrainingDto> viewTraineeTrainingListWithPeriod(TrainingBetweenDates trainingBetweenDates) {
		log.info("invoked viewTraineeTrainingListWithPeriod() with dto: ",trainingBetweenDates);

		Trainee trainee = traineeService.getTraineeByUsername(trainingBetweenDates.getUsername());
		List<Training> list = trainingRepository.findAllTrainingInBetween(trainingBetweenDates.getFrom(),
				trainingBetweenDates.getTo(), trainee);
		return list.stream()
				.map(training -> 
				TraineeTrainingDto.builder()
				.trainingName(training.getTrainingName())
				.trainingTypeId(training.getTrainingType().getId())
				.trainingDate(training.getTrainingDate())
				.trainingDuration(training.getTrainingDuration())
				.trainerUsername(trainerRepository.findById(training.getTrainee().getId()).get().getUser().getFirstName())
				.build()
				)
				.toList();
	}

	public List<TrainerTrainingDto> viewTrainerTrainingListWithPeriod(TrainingBetweenDates trainingBetweenDates) {
		log.info("invoked viewTrainerTrainingListWithPeriod() method dto: ",trainingBetweenDates);
		Trainer trainer = trainerService.getTrainerByUsername(trainingBetweenDates.getUsername());
		List<Training> list = trainingRepository.findAllTrainersTrainingsInBetween(trainingBetweenDates.getFrom(),
				trainingBetweenDates.getTo(), trainer);
		return list.stream()
				.map(training -> 
				TrainerTrainingDto.builder()
				.traineeUsername(traineeRepository.findById(training.getTrainee().getId()).get().getUser().getFirstName())
				.trainingName(training.getTrainingName())
				.trainingDate(training.getTrainingDate())
				.trainingDuration(training.getTrainingDuration())
				.build()
				)
				.toList();
	}
}
