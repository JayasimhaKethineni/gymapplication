package com.epam.gymapp.service;

import com.epam.gymapp.model.User;

public interface LoginService {


	public User authenticate(String username, String password);

	public User changeLogin(String username, String oldPassword, String newPassword);
}
