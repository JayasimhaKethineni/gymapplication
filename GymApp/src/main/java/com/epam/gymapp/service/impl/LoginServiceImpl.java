package com.epam.gymapp.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.gymapp.exception.GymAppException;
import com.epam.gymapp.model.User;
import com.epam.gymapp.repository.UserRepository;
import com.epam.gymapp.service.LoginService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LoginServiceImpl implements LoginService {

	@Autowired
	private UserRepository userRepository;

	public User authenticate(String username, String password) {
		log.info(String.format("invoked authenticate() method with username:%s and password :%s", username,password));
		Optional<User> user = userRepository.findByUserName(username);
		if (!(user.isPresent() && user.get().getPassword().equals(password))) {
			throw new GymAppException("Invalid Credentials!!");
		}
		return user.get();
	}

	public User changeLogin(String username, String oldPassword, String newPassword) {
		log.info(String.format("invoked changeLogin() method with username:%s ,oldpassword: %s , password :%s", username,oldPassword,newPassword));

		Optional<User> user = userRepository.findByUserName(username);
		if (!(user.isPresent() && user.get().getPassword().equals(oldPassword))) {
			throw new GymAppException("Invalid Credentials!!");
		}
		user.get().setPassword(newPassword);
		userRepository.save(user.get());
		return user.get();
	}
}
