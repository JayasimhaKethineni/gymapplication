package com.epam.gymapp.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.epam.gymapp.dto.request.SendMailDto;
import com.epam.gymapp.dto.request.TraineeDto;
import com.epam.gymapp.dto.request.TraineeRegDto;
import com.epam.gymapp.dto.request.UpdateTraineeList;
import com.epam.gymapp.dto.response.ResponseDto;
import com.epam.gymapp.dto.response.TrainerListDto;
import com.epam.gymapp.dto.response.UpdatedTraineeDto;
import com.epam.gymapp.dto.response.ViewTraineeDto;
import com.epam.gymapp.exception.GymAppException;
import com.epam.gymapp.kafka.NotificationProducer;
import com.epam.gymapp.model.Trainee;
import com.epam.gymapp.model.Trainer;
import com.epam.gymapp.model.User;
import com.epam.gymapp.repository.TraineeRepository;
import com.epam.gymapp.repository.TrainerRepository;
import com.epam.gymapp.repository.TrainingRepository;
import com.epam.gymapp.repository.UserRepository;
import com.epam.gymapp.service.TraineeService;
import com.epam.gymapp.service.TrainerService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TraineeServiceImpl implements TraineeService {

	@Autowired
	private TraineeRepository traineeRepository;
	
	@Autowired
	private TrainerRepository trainerRepository;
	
	@Autowired
	private TrainingRepository trainingRepository;

	@Autowired
	private UserRepository userRepository;

//	@Autowired
//	private ModelMapper mapper;

	@Autowired
	private TrainerService trainerService;
	
	@Autowired
	private PasswordEncoder encoder;
	
	
	@Autowired
	private NotificationProducer notificationProducer;

	public ResponseDto addTrainee(TraineeRegDto traineeRegDto) {
		log.info("invoked addTrainee() method with input:",traineeRegDto);

		String password = traineeRegDto.getFirstName() + new Date().hashCode();
		User user= User.builder()
				.firstName(traineeRegDto.getFirstName())
				.lastName(traineeRegDto.getLastName())
				.userName(traineeRegDto.getEmail())
				.password(encoder.encode(password))
				.isActive(true)
				.build();
		Trainee trainee= Trainee.builder()
				.address(traineeRegDto.getAddress())
				.dateOfBirth(traineeRegDto.getDateOfBirth())
				.user(user)
				.build();
		traineeRepository.save(trainee);
		ResponseDto responseDto=ResponseDto.builder().username(traineeRegDto.getEmail()).password(password).build();
		String body="welcome to gym application!! you are registered as trainee \n username: "+responseDto.getUsername()+"\n password: "+responseDto.getPassword()+"\n thank you";
		String subject="registration into gym application!!";
		SendMailDto sendMailDto=new SendMailDto(responseDto.getUsername(), "jayk070707@gmail.com", subject, body);
		
		notificationProducer.sendNotification(sendMailDto);
		return responseDto;
	}

	public Trainee getTraineeByUsername(String username) {
		log.info("invoked getTraineeByUsername() with username:",username);

		User user = userRepository.findByUserName(username)
				.orElseThrow(() -> new GymAppException("enter valid username!!"));
		return traineeRepository.findByUserId(user.getId())
				.orElseThrow(() -> new GymAppException("verify the user properly!!"));
	}

	public ViewTraineeDto getTraineeDtoByUsername(String username) {
		log.info("invoked getTraineeDtoByUsername() with username:",username);

		User user = userRepository.findByUserName(username)
				.orElseThrow(() -> new GymAppException("enter valid username!!"));
		Trainee trainee = traineeRepository.findByUserId(user.getId())
				.orElseThrow(() -> new GymAppException("verify the user properly!!"));
		List<TrainerListDto> trainersDto = trainee.getTrainers().stream()
				.map(trainer -> 
				TrainerListDto.builder()
				.userName(trainer.getUser().getUserName())
				.firstName(trainer.getUser().getFirstName())
				.lastName(trainer.getUser().getLastName())
				.specializationName(trainer.getSpecializationId().getTrainingTypeName())
				.build()
				)
				.toList();
		return ViewTraineeDto.builder()
				.userName(username)
				.firstName(user.getFirstName())
				.lastName(user.getLastName())
				.dateOfBirth(trainee.getDateOfBirth())
				.address(trainee.getAddress())
				.isActive(user.getIsActive())
				.trainers(trainersDto)
				.build();
	}

	public UpdatedTraineeDto updateTrainee(TraineeDto traineeDto) {
		log.info("invoked updateTrainee() with input",traineeDto);

		Trainee trainee = getTraineeByUsername(traineeDto.getUserName());
		trainee.setAddress(traineeDto.getAddress());
		trainee.setDateOfBirth(traineeDto.getDateOfBirth());
		trainee.getUser().setFirstName(traineeDto.getFirstName());
		trainee.getUser().setLastName(traineeDto.getLastName());
		trainee.getUser().setIsActive(traineeDto.isActive());
		
		traineeRepository.save(trainee);
//		UpdatedTraineeDto updatedTraineeDto = mapper.map(traineeDto, UpdatedTraineeDto.class);
		UpdatedTraineeDto updatedTraineeDto = UpdatedTraineeDto.builder()
				.userName(traineeDto.getUserName())
				.firstName(traineeDto.getFirstName())
				.lastName(traineeDto.getLastName())
				.address(traineeDto.getAddress())
				.dateOfBirth(traineeDto.getDateOfBirth())
				.isActive(traineeDto.isActive())
				.build();

		updatedTraineeDto.setTrainersList(trainee.getTrainers().stream()
				.map(trainer -> 
				TrainerListDto.builder().userName(trainer.getUser().getUserName())
				.firstName(trainer.getUser().getFirstName())
				.lastName(trainer.getUser().getLastName())
				.specializationName(trainer.getSpecializationId().getTrainingTypeName())
				.build()
				)
				.toList());
		notificationProducer.sendNotification(SendMailDto.builder().toMail(updatedTraineeDto.getUserName())
				.ccMail("jj@gmail.com").body("your profile is updated!!").subject("Update alert!!!").build());
				
		return updatedTraineeDto;
	}

	public void deleteTrainee(String username) {
		log.info("invoked deleteTrainee() with username: ",username);
		Trainee trainee = getTraineeByUsername(username);
		traineeRepository.delete(trainee);
//		trainingRepository.deleteByTrainee(trainee);
	}

	public List<TrainerListDto> updateList(UpdateTraineeList updateTraineeList) {
		log.info("invoked updateList() method");

		Trainee trainee = getTraineeByUsername(updateTraineeList.getUsername());
		List<Trainer> trainers = updateTraineeList.getTrainers().stream()
				.map(name -> trainerService.getTrainerByUsername(name)).toList();
		trainee.getTrainers().clear();
		trainee.getTrainers().addAll(trainers);
		traineeRepository.save(trainee);

		return trainers.stream()
				.map(trainer -> 
				TrainerListDto.builder().userName(trainer.getUser().getUserName())
				.firstName(trainer.getUser().getFirstName())
				.lastName(trainer.getUser().getLastName())
				.specializationName(trainer.getSpecializationId().getTrainingTypeName())
				.build()
				).toList();
	}

	public List<TrainerListDto> notAssignedTrainersToTrainee(String username) {
		log.info("invoked notAssignedTrainersToTrainee() method !!!");
		Trainee trainee = getTraineeByUsername(username);
		List<Trainer> assignedTrainers = trainee.getTrainers();
		List<Trainer> allTrainers = trainerRepository.findAll();
		allTrainers.removeAll(assignedTrainers);

		return allTrainers.stream() 
				.map(trainer -> 
				TrainerListDto.builder().userName(trainer.getUser().getUserName())
				.firstName(trainer.getUser().getFirstName())
				.lastName(trainer.getUser().getLastName())
				.specializationName(trainer.getSpecializationId().getTrainingTypeName())
				.build()
				)
				.toList();

	}

}
