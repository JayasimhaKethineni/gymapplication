package com.epam.gymapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.gymapp.model.TrainingType;

public interface TrainingTypeRepository extends JpaRepository<TrainingType, Integer> {
	
}
