package com.epam.gymapp.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.epam.gymapp.model.Trainee;
import com.epam.gymapp.model.Trainer;
import com.epam.gymapp.model.Training;

public interface TrainingRepository extends JpaRepository<Training, Integer> {

	List<Training> getAllByTrainerId(Integer id);
	
	@Query("SELECT t FROM Training t WHERE (:from IS NULL OR t.trainingDate >= :from) AND (:to IS NULL OR t.trainingDate < :to) AND t.trainee = :trainee")
	List<Training> findAllTrainingInBetween(@Param("from") Date from, @Param("to") Date to, @Param("trainee") Trainee trainee);
	
	@Query("SELECT t FROM Training t WHERE (:from IS NULL OR t.trainingDate >= :from) AND (:to IS NULL OR t.trainingDate < :to) AND t.trainer = :trainer")
	List<Training> findAllTrainersTrainingsInBetween(@Param("from") Date from, @Param("to") Date to, @Param("trainer") Trainer trainer);

	void deleteByTrainee(Trainee trainee);
}
