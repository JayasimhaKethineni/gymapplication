package com.epam.integration.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.integration.entity.User;
import com.epam.integration.repository.UserRepository;
import com.epam.integration.service.LoginService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LoginServiceImpl implements LoginService {

	@Autowired
	private UserRepository userRepository;

	public User authenticate(String username, String password) {
		Optional<User> user = userRepository.findByUserName(username);
		if (!(user.isPresent() && user.get().getPassword().equals(password))) {
			throw new RuntimeException("Invalid Credentials!!");
		}
		return user.get();
	}

	public User changeLogin(String username, String oldPassword, String newPassword) {

		Optional<User> user = userRepository.findByUserName(username);
		if (!(user.isPresent() && user.get().getPassword().equals(oldPassword))) {
			throw new RuntimeException("Invalid Credentials!!");
		}
		user.get().setPassword(newPassword);
		userRepository.save(user.get());
		return user.get();
	}
}
