package com.epam.integration.service;

import com.epam.integration.entity.User;

public interface LoginService {


	public User authenticate(String username, String password);

	public User changeLogin(String username, String oldPassword, String newPassword);
}
