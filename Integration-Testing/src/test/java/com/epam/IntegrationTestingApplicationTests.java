package com.epam;


import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.epam.integration.entity.User;
import com.epam.integration.service.LoginService;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;


@CucumberContextConfiguration
@SpringBootTest
public class IntegrationTestingApplicationTests {
	private String username;
    private String password;
    private User authenticatedUser;
    @Autowired
    private LoginService userService ; 

    @Given("there is a user with username {string} and password {string}")
    public void there_is_a_user_with_username_and_password(String string, String string2) {
    	this.username = string;
        this.password = string2;
    }
    @When("I authenticate with username {string} and password {string}")
    public void i_authenticate_with_username_and_password(String string, String string2) {
    	try {
            authenticatedUser = userService.authenticate(string, string2);
        } catch (RuntimeException e) {
            // Catch any authentication exception if thrown
        }
    }
    @Then("the authentication should be successful")
    public void the_authentication_should_be_successful() {
    	assertNotNull(authenticatedUser);
    }

    @Then("the authentication should fail")
    public void the_authentication_should_fail() {
    	assertNull(authenticatedUser);
    }

}
