Feature: User Authentication

  Scenario: Valid user authentication
    Given there is a user with username "krishna@gmail.com" and password "123"
    When I authenticate with username "krishna@gmail.com" and password "123"
    Then the authentication should be successful

  Scenario: Invalid user authentication
    Given there is a user with username "krishna@gmail.com" and password "1"
    When I authenticate with username "krishna@gmail.com" and password "1"
    Then the authentication should fail
