package com.epam.reportservice.entity;

import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "report-service")
public class TrainingSummary {
	
	@Id
	private String trainerUsername;
	private String trainerFirstName;
	private String trainerLastName;
	private String status;
	private Map<Integer,Map<Integer,Map<Integer,Integer>>> yearSummary;

}
