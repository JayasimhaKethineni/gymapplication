package com.epam.reportservice.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.epam.reportservice.entity.TrainingSummary;

public interface TrainingReportRepository extends MongoRepository<TrainingSummary, String> {

	Optional<TrainingSummary> findByTrainerUsername(String trainerUsername);
}
