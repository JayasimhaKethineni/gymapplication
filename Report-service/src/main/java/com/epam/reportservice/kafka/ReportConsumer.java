package com.epam.reportservice.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.epam.gymapp.dto.request.TrainingSummaryDto;
import com.epam.reportservice.service.ReportService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ReportConsumer {

	@Autowired
	private ReportService reportService;
	
	@KafkaListener(topics = "reports", groupId = "myGroup")
	public void consumer(TrainingSummaryDto trainingSummaryDto) {
		reportService.updateReport(trainingSummaryDto);
	}
}
