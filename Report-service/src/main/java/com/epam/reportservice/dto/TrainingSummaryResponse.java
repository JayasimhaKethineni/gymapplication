package com.epam.reportservice.dto;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TrainingSummaryResponse {

	private String trainerUsername;
	private String trainerFirstName;
	private String trainerLastName;
	private String status;
	private Map<Integer,Map<Integer,Map<Integer,Integer>>> yearSummary;
}
