package com.epam.reportservice.dto;

import java.util.Date;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TrainingSummaryDto {

	private String trainingName;
	private Date trainingDate;
	private int trainingDuration;
	private String trainerUsername;
	private String trainerFirstName;
	private String trainerLastName;
	
}
