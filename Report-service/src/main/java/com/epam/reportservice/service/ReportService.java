package com.epam.reportservice.service;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.gymapp.dto.request.TrainingSummaryDto;
import com.epam.reportservice.dto.TrainingSummaryResponse;
import com.epam.reportservice.entity.TrainingSummary;
import com.epam.reportservice.repository.TrainingReportRepository;

@Service
public class ReportService {

	@Autowired
	private TrainingReportRepository trainingReportRepository;
	
	
	public void updateReport(TrainingSummaryDto trainingSummaryDto) {
		
		TrainingSummary trainingSummary=trainingReportRepository.findById(trainingSummaryDto.getTrainerUsername())
				.orElse(TrainingSummary.builder().trainerUsername(trainingSummaryDto.getTrainerUsername())
						.trainerFirstName(trainingSummaryDto.getTrainerFirstName())
						.trainerLastName(trainingSummaryDto.getTrainerLastName())
						.yearSummary(new HashMap<>())
						.build());
		System.out.println(trainingSummary.toString());
		Map<Integer,Map<Integer,Map<Integer,Integer>>> newResult=trainingSummary.getYearSummary();
		Calendar calendar=Calendar.getInstance();
		calendar.setTime(trainingSummaryDto.getTrainingDate());
		
		int month=calendar.get(Calendar.MONTH)+1;
		System.out.println(month);
		int year=calendar.get(Calendar.YEAR);
		System.out.println(year);
		int day=calendar.get(Calendar.DAY_OF_MONTH);
		System.out.println(day);
		Map<Integer, Map<Integer, Integer>> yearReport=newResult.getOrDefault(year, new HashMap<>());
		Map<Integer,Integer> monthReport=yearReport.getOrDefault(month,new HashMap<>());
		monthReport.put(day, monthReport.getOrDefault(day, 0)+trainingSummaryDto.getTrainingDuration());
		yearReport.put(month, monthReport);
		newResult.put(year, yearReport);
		
		trainingSummary.setYearSummary(newResult);
		trainingReportRepository.save(trainingSummary);
	}
	
	public TrainingSummaryResponse getReport(String username) {
		TrainingSummary trainingSummary=trainingReportRepository.findById(username).orElseThrow(()->new RuntimeException("no username found!!"));
		return TrainingSummaryResponse.builder()
				.trainerFirstName(trainingSummary.getTrainerFirstName())
				.trainerLastName(trainingSummary.getTrainerLastName())
				.trainerUsername(trainingSummary.getTrainerUsername())
				.yearSummary(trainingSummary.getYearSummary())
				.status(trainingSummary.getStatus())
				.build();
	}
}
