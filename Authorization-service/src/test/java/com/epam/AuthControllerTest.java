package com.epam;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.authorization.controller.AuthController;
import com.epam.authorization.dto.AuthRequest;
import com.epam.authorization.service.AuthService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(controllers = AuthController.class, excludeAutoConfiguration = {SecurityAutoConfiguration.class})
class AuthControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthService authService;

    @MockBean
    private AuthenticationManager authenticationManager;
    

    @Test
    void testGetTokenInValidCredentials() throws Exception {
        AuthRequest authRequest = new AuthRequest("username", "password");
        Authentication authentication = new UsernamePasswordAuthenticationToken("username", "password");
        
        when(authenticationManager.authenticate(any()))
                .thenReturn(authentication);

        mockMvc.perform(post("/auth/token")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(authRequest)))
                .andExpect(status().isBadRequest());
        
        verify(authenticationManager).authenticate(any());
    }
    
    @Test
    void testGetTokenValidCredentialsReturnsToken() throws Exception {
        AuthRequest authRequest = new AuthRequest("username", "password");
        String token = "sampleToken";
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

        Authentication authentication = new UsernamePasswordAuthenticationToken("username", "password",authorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        when(authenticationManager.authenticate(any()))
                .thenReturn(SecurityContextHolder.getContext().getAuthentication());
        when(authService.generateToken(authRequest.getUsername())).thenReturn(token);

        mockMvc.perform(post("/auth/token")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(authRequest)))
                .andExpect(status().isOk());
        
        verify(authenticationManager).authenticate(any());
        verify(authService).generateToken(authRequest.getUsername());
    }

    @Test
    void testValidateTokenValidTokenReturnsOk() throws Exception {
        String validToken = "validToken";

       Mockito.doNothing().when(authService).validateToken(validToken);

        mockMvc.perform(get("/auth/validate")
                .param("token", validToken))
                .andExpect(status().isOk());
        
        verify(authService).validateToken(validToken);
    }

    @Test
    void testValidateTokenInvalidTokenReturnsUnauthorized() throws Exception {
        String invalidToken = "invalidToken";

        Mockito.doThrow(RuntimeException.class).when(authService).validateToken(invalidToken);

        mockMvc.perform(get("/auth/validate")
                .param("token", invalidToken))
                .andExpect(status().isBadRequest());
        
        verify(authService).validateToken(invalidToken);
    }
    
    @Test
    void testGetTokenWithInvalidRequest() throws Exception {
        AuthRequest authRequest = new AuthRequest("username", "password");
        authRequest.setPassword(null);

        mockMvc.perform(post("/auth/token")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(authRequest)))
                .andExpect(status().isBadRequest());
        
    }
    
    @Test
    void testGetTokenWithException() throws Exception {
        AuthRequest authRequest = new AuthRequest("username", "password");
        
        when(authenticationManager.authenticate(any()))
                .thenThrow(RuntimeException.class);

        mockMvc.perform(post("/auth/token")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(authRequest)))
                .andExpect(status().isBadRequest());
        
//        verify(authenticationManager).authenticate(any());
    }
}


