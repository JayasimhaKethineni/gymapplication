package com.epam;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.authorization.service.JwtService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;
@ExtendWith(MockitoExtension.class)
class JwtServiceTest {

    @InjectMocks
    private JwtService jwtService;

    @Mock
    private Keys keysMock;
    
    String userName;
    
    String token;
    
    @BeforeEach
    public void setup() {
    	userName = "testUser";

        token = jwtService.generateToken(userName);
    }

    @Test
    void testGenerateToken() {
        assertNotNull(token);
    }

    @Test
    void testValidateToken_ValidToken() {
        assertDoesNotThrow(() -> jwtService.validateToken(token));
    }

    @Test
    void testValidateToken_InvalidToken() {
        String invalidToken = "invalidToken";

        assertThrows(MalformedJwtException.class, () -> jwtService.validateToken(invalidToken));
    }

    @Test
    void testValidateToken_ExpiredToken() {
        String userName = "testUser";
        Map<String, Object> claims = new HashMap<>();
        claims.put("sub", userName);

        Key signKey = Keys.secretKeyFor(SignatureAlgorithm.HS256);

        String token = Jwts.builder()
                .setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() - 1000))
                .signWith(signKey)
                .compact();

//        assertThrows(ExpiredJwtException.class, () -> jwtService.validateToken(token));
        assertThrows(SignatureException.class, () -> jwtService.validateToken(token));

    }
}