package com.epam;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.epam.authorization.entity.User;
import com.epam.authorization.repository.UserRepository;
import com.epam.authorization.service.AuthService;
import com.epam.authorization.service.JwtService;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;

@ExtendWith(MockitoExtension.class)
public class AuthServiceTest {

    @Mock
    private UserRepository userRepositoryMock;

    @Mock
    private PasswordEncoder passwordEncoderMock;

    @Mock
    private JwtService jwtServiceMock;

    @InjectMocks
    private AuthService authService;

    @Test
    void testSaveUser() {
        User user = new User();
        user.setUserName("testUser");
        user.setPassword("testPassword");

        when(passwordEncoderMock.encode(any())).thenReturn("encodedPassword");
        when(userRepositoryMock.save(any())).thenReturn(user);

        String result = authService.saveUser(user);

        verify(passwordEncoderMock).encode("testPassword");
        verify(userRepositoryMock).save(user);

        assertEquals("user added to the system", result);
    }

    @Test
    void testGenerateToken() {
        when(jwtServiceMock.generateToken("testUser")).thenReturn("generatedToken");

        String result = authService.generateToken("testUser");

        verify(jwtServiceMock).generateToken("testUser");

        assertEquals("generatedToken", result);
    }

    @Test
    void testValidateToken_ValidToken() {
        String token = "valid-token";

        // Mocking successful token validation
        doNothing().when(jwtServiceMock).validateToken(token);

        // Call the method and assert that it does not throw an exception
        assertDoesNotThrow(() -> authService.validateToken(token));

        verify(jwtServiceMock).validateToken(token);
    }

    @Test
    void testValidateToken_ExpiredToken() {
        String token = "expired-token";

        doThrow(ExpiredJwtException.class).when(jwtServiceMock).validateToken(token);

        assertThrows(ExpiredJwtException.class, () -> authService.validateToken(token));

        verify(jwtServiceMock).validateToken(token);
    }

    @Test
    void testValidateToken_MalformedToken() {
        String token = "malformed-token";

        doThrow(MalformedJwtException.class).when(jwtServiceMock).validateToken(token);

        assertThrows(MalformedJwtException.class, () -> authService.validateToken(token));

        verify(jwtServiceMock).validateToken(token);
    }

    @Test
    void testValidateToken_InvalidSignature() {
        String token = "invalid-signature-token";

        doThrow(SignatureException.class).when(jwtServiceMock).validateToken(token);

        assertThrows(SignatureException.class, () -> authService.validateToken(token));

        verify(jwtServiceMock).validateToken(token);
    }
}

