package com.epam.authorization.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

	@ExceptionHandler(AuthException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ExceptionResponse exceptionHandler(AuthException e, WebRequest re) {
		log.info("GymAppException occured!!!");
		return new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.toString(), e.getMessage(), re.getContextPath());
	}
	
	
	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ExceptionResponse dtoValidation(RuntimeException e, WebRequest re) {
		log.info("RuntimeException occured!!!");

		return new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(), e.getMessage(),
				re.getContextPath());
	}


}
