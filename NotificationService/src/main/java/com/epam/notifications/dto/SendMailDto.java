package com.epam.notifications.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class SendMailDto {

	private String toMail;
	private String ccMail;
	private String subject;
	private String body;
	
}
