package com.epam.notifications.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.epam.notifications.models.Mail;

public interface MailRepository extends MongoRepository<Mail, String>{

	
}
