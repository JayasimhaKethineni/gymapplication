package com.epam.notifications.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.gymapp.dto.request.SendMailDto;
import com.epam.notifications.service.MailService;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;


@RestController
@RequestMapping("/mails")
@Slf4j
public class MailController {
	
	@Autowired
	private MailService mailService;
	
	@PostMapping
	public ResponseEntity<Void> sendMailDto(@RequestBody @Valid SendMailDto sendMailDto) {
		log.info("invoked sendMailDto() controller!!");
		mailService.sendEmail(sendMailDto);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
