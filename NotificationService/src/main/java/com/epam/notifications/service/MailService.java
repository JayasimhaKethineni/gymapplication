package com.epam.notifications.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.epam.gymapp.dto.request.SendMailDto;
import com.epam.notifications.models.Mail;
import com.epam.notifications.repository.MailRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MailService {

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private MailRepository mailRepository;
	
	@Value("${app.default.mail}")
	private String defaultMail;
	
	public void sendEmail(SendMailDto sendMailDto) {
		log.info("invoked sendEmail() method!! with content :",sendMailDto);
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(sendMailDto.getToMail());

        msg.setSubject(sendMailDto.getSubject());
        msg.setText(sendMailDto.getBody());
        msg.setCc(sendMailDto.getCcMail());
        Mail mail= Mail.builder().fromEmail(defaultMail)
        		.toEmail(sendMailDto.getToMail())
        		.body(sendMailDto.getBody())
        		.status("success")
        		.remarks("mail sent ..")
        		.build();
        
        try {
        	javaMailSender.send(msg);
	        log.info("in try block!!!");
        }catch(MailException e) {
        	mail.setStatus("failed");
        	mail.setRemarks(e.getMessage());
        	log.info("in catch block");
        }

        log.info(mail.toString());
        mailRepository.save(mail);
    }
}
