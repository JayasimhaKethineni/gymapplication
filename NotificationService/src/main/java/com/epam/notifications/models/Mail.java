package com.epam.notifications.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Document(collection = "service")
public class Mail {
	
	String fromEmail;
	String toEmail;
	String ccEmail;
	String body;
	String status;
	String remarks;
//	public Mail(String fromEmail, String toEmail, String ccEmail, String body, String status, String remarks) {
//		super();
//		this.fromEmail = fromEmail;
//		this.toEmail = toEmail;
//		this.ccEmail = ccEmail;
//		this.body = body;
//		this.status = status;
//		this.remarks = remarks;
//	}
	

}
