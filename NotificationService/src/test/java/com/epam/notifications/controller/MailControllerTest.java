package com.epam.notifications.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.epam.gymapp.dto.request.SendMailDto;
import com.epam.notifications.controllers.MailController;
import com.epam.notifications.service.MailService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(MailController.class)
class MailControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private MailService mailService;

	@Test
	void testSendMailDto_ValidDto_ReturnsCreated() throws Exception {
		SendMailDto sendMailDto = new SendMailDto();
        sendMailDto.setToMail("test@gmail.com");
        sendMailDto.setSubject("Test Subject");
        sendMailDto.setBody("Test Body");
        sendMailDto.setCcMail("cc@gmail.com");
        
		ObjectMapper map = new ObjectMapper();
		String jsonForm = map.writeValueAsString(sendMailDto);
		mockMvc.perform(MockMvcRequestBuilders.post("/mails").contentType(MediaType.APPLICATION_JSON).content(jsonForm))
				.andExpect(MockMvcResultMatchers.status().isOk());

		Mockito.verify(mailService, times(1)).sendEmail(sendMailDto);
	}

	@Test
	void testSendMailDto_NullDto_ReturnsBadRequest() throws Exception {
		ObjectMapper map = new ObjectMapper();
		String jsonForm = map.writeValueAsString(null);
		mockMvc.perform(MockMvcRequestBuilders.post("/mails").contentType(MediaType.APPLICATION_JSON).content(jsonForm))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());

		Mockito.verify(mailService, never()).sendEmail(any());
	}
}
