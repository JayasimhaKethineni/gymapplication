package com.epam.notifications.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import com.epam.gymapp.dto.request.SendMailDto;
import com.epam.notifications.models.Mail;
import com.epam.notifications.repository.MailRepository;

@ExtendWith(MockitoExtension.class)
class MailServiceTest{
	
	@InjectMocks 
	private MailService mailService;
	@Mock
	private JavaMailSender javaMailSender;
	@Mock
	private MailRepository mailRepository;
	
	@Test
    void testSendEmailSuccess() {
        SendMailDto sendMailDto = new SendMailDto();
        sendMailDto.setToMail("test@gmail.com");
        sendMailDto.setSubject("Test Subject");
        sendMailDto.setBody("Test Body");
        sendMailDto.setCcMail("cc@gmail.com");

        Mockito.doNothing().when(javaMailSender).send(any(SimpleMailMessage.class));

        mailService.sendEmail(sendMailDto);

        Mockito.verify(javaMailSender, times(1)).send(any(SimpleMailMessage.class));

        Mockito.verify(mailRepository, times(1)).save(any(Mail.class));
    }
	@Test
    void testSendEmailFailure() {
        SendMailDto sendMailDto = new SendMailDto();
        sendMailDto.setToMail("test@gmail.com");
        sendMailDto.setSubject("Test Subject");
        sendMailDto.setBody("Test Body");
        sendMailDto.setCcMail("cc@gmail.com");

        Mockito.doThrow(new MailSendException("Mail sending failed")).when(javaMailSender).send(any(SimpleMailMessage.class));

        mailService.sendEmail(sendMailDto);

        Mockito.verify(javaMailSender, times(1)).send(any(SimpleMailMessage.class));

        Mockito.verify(mailRepository, times(1)).save(any(Mail.class));
	}
}